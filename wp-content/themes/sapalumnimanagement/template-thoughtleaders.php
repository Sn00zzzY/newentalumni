<?php
/**
 * Template Name:  Template Thoughtleaders
 */
get_header(); ?>

<main class="main">
  <?php get_template_part( 'template-parts/hero' ); ?>
  <?php get_template_part( 'template-parts/company-list' ); ?>
</main>

<?php get_footer(); ?>