<?php
	/**
	 * Template Name: General Page Template
	 */

	get_header(); 
?>

<main class="main">
	<section class="promo-section section-general" id="home">
	</section>
	<div class="content-holder content-general container">
		<div class="content">
			<div class="holder">
				<div class="text-holder">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
</main>

<?php get_footer(); ?>
