<?php
/**
 * Template Name: Home Template
 */
get_header(); ?>

<main class="main">

    <?php get_template_part( 'template-parts/homeslider' ); ?>
	<?php //get_template_part( 'template-parts/hero' ); ?>
    <?php //get_template_part( 'template-parts/hometabheader' ); ?>
	
    <div class="tab-content">
        <div id="large-org" class="tab-pane fade in active">
        	<?php get_template_part( 'template-parts/about' ); ?>
			<?php get_template_part( 'template-parts/features' ); ?>
			<?php get_template_part( 'template-parts/services' ); ?>
			<?php get_template_part( 'template-parts/business' ); ?>
			<?php get_template_part( 'template-parts/modules' ); ?>
			<?php get_template_part( 'template-parts/integration' ); ?>
			<?php get_template_part( 'template-parts/contacts' ); ?>
			<?php get_template_part( 'template-parts/advpart'); ?>
        </div>
        <!-- <div id="small-org" class="tab-pane fade">
            <?php //get_template_part( 'template-parts/smbcontactmodal' ); ?>
		    <?php //get_template_part( 'template-parts/newhomebanner' ); ?>
		    <?php //get_template_part( 'template-parts/newfeatures' ); ?>
		    <?php //get_template_part( 'template-parts/newpowerfeaturelist' ); ?>
		    <?php //get_template_part( 'template-parts/newservices' ); ?>
		    <?php //get_template_part( 'template-parts/newcontactbar' ); ?>
        </div> -->
    </div>
</main>

<?php get_footer(); ?>