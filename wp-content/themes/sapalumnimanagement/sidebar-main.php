<?php
/**
 * The template for the sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage SAP_Alumni
 * @since SAP Alumni 1.0
 */
?>

<aside class="sidebar <?php if(is_single()) { echo 'single-post'; } ?> <?php if(!get_the_post_thumbnail_url()) { echo 'no-image'; } ?>">
	<?php if (is_single() && get_the_post_thumbnail_url()) :?>
		<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="img" class="img-responsive thumbnail-img">
	<?php endif; ?>

	<div class="widget-list-wrapper">
		<div class="search-widget widget-wrapper">
			<form role="search" method="get" id="searchform" class="searchform search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
				<input class="form-control" placeholder="Search" type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" />
				<i class="fa fa-search"></i>
			</form>
		</div>
		<?php if(!is_single()) : ?>
			<div class="archive-widget dropdown widget-wrapper">
				<select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
					<option value="">Archive</option>
					<?php wp_get_archives( array( 'type' => 'monthly', 'format' => 'option', 'show_post_count' => 1 ) ); ?>
				</select>
			</div>
		<?php endif; ?>

		<div class="post-widget widget-wrapper">
			<?php
				$args = array(
					'numberposts' => 7,
					'offset' => 0,
					'category' => 0,
					'orderby' => 'post_date',
					'order' => 'DESC',
					'include' => '',
					'exclude' => '',
					'meta_key' => '',
					'meta_value' =>'',
					'post_type' => 'post',
					'post_status' => 'publish',
					'suppress_filters' => true
				);

			$recent_posts = wp_get_recent_posts( $args, ARRAY_A ); ?>

			<?php if ( count( $recent_posts ) > 1 ) : ?>
				<?php $i=0; ?>
				<h2>Recent Posts</h2>

				<?php foreach ( $recent_posts as $rpost ) { ?>
					<?php if($rpost[ID] != $post->ID): ?>
						<div class="posts-holder">
							<div class="description">
								<a href="<?php echo get_permalink($rpost[ID]); ?>"><?php echo $rpost[post_title]; ?></a>
							</div>
						</div>
					<?php endif; ?>
					<?php $i++; ?>
				<?php } ?>
			<?php endif; ?>

		</div>

		<div class="categories-widget widget-wrapper">
			<?php $update_category = 3;?>
			<h2>Categories</h2>
			<ul class="categories-list">
				<?php $categories = get_categories( array(
					'orderby' => 'name',
					'order'   => 'ASC',
					'exclude' => array($update_category, 1)
				) );
				?>

				<?php foreach( $categories as $category ): ?>
				<?php $cat = get_category( $category ); ?>
				<li>
					<a href="/category/<?php echo $cat->slug; ?>">
						<div class="icon-holder">
							<i class="<?php echo $cat_data = get_option('category_' . $category->cat_ID )[img]; ?>" aria-hidden="true"></i>
						</div><?php echo $category->name; ?>
					</a>
				</li>
				<?php endforeach; ?>

				<li class="update-category-link">
					<a href="/category/<?php echo get_category($update_category)->slug; ?>">
						<div class="icon-holder">
							<i class="<?php echo get_option('category_' . $update_category )[img]; ?>" aria-hidden="true"></i>
						</div><?php echo get_category($update_category)->name; ?>
					</a>
				</li>
			</ul>
		</div>
	</div>
</aside>
