<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage SAP_Alumni
 * @since SAP Alumni 1.0
 */

get_header(); ?>

<main class="main clearfix">
	<div class="content-holder container">

		<div class="content">
			<h1 class="blog-title">Alumni Management Blog</h1>

			<?php if ( have_posts() ) : ?>

			<div class="post-list">
				<?php
                    // Start the Loop.
                    while ( have_posts() ) : the_post();
			
			$categories = get_the_category($post->ID);

			foreach ($categories as $category) {
				if ($category->term_id === 1 || $category->name === 'Uncategorized') {
					continue 2;
				}
			}

                        /*
                            * Include the Post-Format-specific template for the content.
                            * If you want to override this in a child theme, then include a file
                            * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                        */
                        get_template_part( 'template-parts/content', get_post_format() );

                    // End the loop.
                    endwhile;
                ?>
			</div>

			<?php
                    // Previous/next page navigation.
                    the_posts_pagination( array(
                        'screen_reader_text' => __( 'Page:', 'sapalumni' ),
			'prev_text'          => __( '<', 'sapalumni' ),
			'next_text'          => __( '>', 'sapalumni' ),
			'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( '', 'sapalumni' ) . ' </span>',
			) );
			?>
			<?php endif; ?>
		</div>

		<?php get_sidebar('main'); ?>
	</div>
</main>

<?php get_footer(); ?>