<?php
/**
 * Template Name: Notification Template
 */
get_header(); ?>

<main class="main">
  <?php get_template_part( 'template-parts/notification-promo-section' ); ?>
  <?php get_template_part( 'template-parts/notification-section' ); ?>
</main>

<?php get_footer(); ?>