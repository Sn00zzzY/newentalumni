<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage SAP_Alumni
 * @since SAP Alumni 1.0
 */
?>

		<footer class="footer">
			<div class="footer-top">
				<div class="container">
					<div class="row">
						<div class="col-md-7">
							<p class="promo-text">EnterpriseJungle is proud to be:</p>
							<div class="logo-holder">
								<?php if( have_rows('proud_to_be_logo_list', option) ): ?>
									<?php while ( have_rows('proud_to_be_logo_list', option) ) : the_row(); ?>
				
										<img src="<?php echo get_sub_field('proud_to_be_logo', option); ?>" alt="partner-logo">
				
									<?php endwhile; ?>
								<?php else : ?>
								<!--// no rows found-->
								<?php endif; ?>

								<a href="<?php echo get_field('thought_leader_logo_link', option); ?>" class="th-logo">
									<img src="<?php echo get_field('thought_leader_logo', option); ?>" alt="thought-leader-logo">
								</a>
								<a href="http://discovery.ariba.com/profile/AN01022658004" class="ariba-logo" target="_blank">
									<img alt="View EnterpriseJungle, Inc profile on Ariba Discovery" border=0 src="http://service.ariba.com/an/p/Ariba/badge_32x32.jpg">
								</a>
							</div>
						</div>
						<div class="col-md-5">
							<div class="social-content">
								<p class="promo-text">Follow us:</p>
								<div class="like-button">
									<script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
									<script type="IN/FollowCompany" data-id="17948480" data-counter="right"></script>
								</div>
								<div class="like-button">
									<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FEnterpriseAlumni%2F&width=89&layout=button_count&action=like&size=small&show_faces=true&share=false&height=21&appId=572549052919640" width="89" height="21" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
								</div>
								<div class="like-button">
									<?php echo do_shortcode("[twitter_follow screen_name='entalumni' show_screen_name='false']"); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-bottom">
				<div class="container">
					<div class="row">
						<div class="col-md-9">
							<div class="copyright">
								<?php echo get_field('copyright', option); ?>
							</div>
						</div>
						<div class="col-md-3">
							<div class="holder">
								<a href="<?php echo get_page_link(467);?>" class="notification">Service Notifications <span class="status"><i class="fa fa-check" aria-hidden="true"></i></span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>

		<?php wp_footer(); ?>
		<div class="mobile-sidebar-overlay visible-xs"></div>
	</div>
</body>
</html>