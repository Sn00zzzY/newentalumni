/*
 Third party
 */

//= ../vendor/jquery/dist/jquery.min.js
//= ../vendor/jquery-validation/dist/jquery.validate.min.js
//=../vendor/slick-carousel/slick/slick.min.js

function syncSlides(slide) {
    $('.notebook ul').slick('slickGoTo', slide);
    $('.first-slider .text ul').slick('slickGoTo', slide);
    $('.second-slider ul').slick('slickGoTo', slide);
    $('.second-slider ul a').removeClass('active');
    // var slides = $('.second-slider ul a');
    // for (var i = 0; slides.length; i++) {
    //     if (parseInt($(slides[i]).parent().attr('data-slide')) === slide) {
    //         $(slides[i]).addClass('active');
    //     }
    // }
    $('.second-slider ul li:nth-child('+ (slide + 1) +') a').addClass('active');
}
function initSliders() {
    $('.notebook ul').slick({
        slide: '.notebook ul li',
        arrows: false,
        infinite: true
    });

    $('.first-slider .text ul').slick({
        slide: '.first-slider .text ul li',
        arrows: false,
        infinite: true
    });

    $('.second-slider ul').slick({
        slide: '.second-slider ul li',
        arrows: false,
        slidesToShow: 6,
        slidesToScroll: 1,
        infinite: true,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
    });

    $('.notebook ul, .second-slider ul, .first-slider .text ul').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        syncSlides(nextSlide);
    });
    $('.second-slider ul').on('click', 'a', function (e) {
        syncSlides(parseInt($(this).parent().data('slide')));
        // $('.second-slider ul a').removeClass('active');
        // $(this).addClass('active');
    });
    // console.debug('Init Sliders!');
}
//function for change share icon to font-awesome icons
function changeIcons(){
    console.log('initIcons');
    if($('.addtoany_shortcode').length) {
        var icons = $('.addtoany_shortcode').find('a[class*=a2a_button_]');
        for (var i = 0; i < icons.length; i++) {
            var iconName = $(icons[i]).attr('class').split('a2a_button_')[1];
            $(icons[i]).html(getIconHtml(iconName));
        }
    }
}
//function return html from icon name
function getIconHtml(term) {
    if(term) {
        var html = '';
        switch (term) {
            case 'facebook':
                html = '<i class="fa fa-facebook-square" aria-hidden="true"></i>';
                break;
            case 'linkedin' :
                html = '<i class="fa fa-linkedin-square" aria-hidden="true"></i>';
                break;
            case  'twitter' :
                html = '<i class="fa fa-twitter-square" aria-hidden="true"></i>';
                break;
            default :
                break;
        }
        return html;
    }
}

$(document).ready(function() {

    $('.btn-toggle, .mobile-sidebar-overlay').on('click', function () {
        toggleSidebar();
    });

    function toggleSidebar() {
        $('.layout').toggleClass('sidebar-open');
    }

	$('a[href^="#"], a[href^="."]').click( function(){ 
        var scroll_el = $(this).attr('href');
        if ($(scroll_el).length != 0) {
            $('html, body').animate({ scrollTop: $(scroll_el).offset().top }, 900); 
        }
        return false; 
    });

    initSliders();
    changeIcons();

});