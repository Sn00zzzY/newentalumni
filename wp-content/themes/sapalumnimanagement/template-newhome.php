<?php
/**
 * Template Name: New Home Template
 */
get_header(); ?>

<main class="main">
    <?php get_template_part( 'template-parts/smbcontactmodal' ); ?>
    <?php get_template_part( 'template-parts/newhomebanner' ); ?>
    <?php get_template_part( 'template-parts/newfeatures' ); ?>
    <?php get_template_part( 'template-parts/newpowerfeaturelist' ); ?>
    <?php get_template_part( 'template-parts/newservices' ); ?>
    <?php get_template_part( 'template-parts/newcontactbar' ); ?>
</main>


<?php get_footer(); ?>