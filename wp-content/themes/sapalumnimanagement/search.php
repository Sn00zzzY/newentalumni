<?php
/**
 * The template for displaying search results pages
 *
 * @package WordPress
 * @subpackage SAP_Alumni
 * @since SAP Alumni 1.0
 */

get_header(); ?>
	<main class="main clearfix">
		<div class="content-holder container">
		<div class="content">

		<?php if ( have_posts() ) : ?>

			<h1 class="blog-title"><?php printf( __( 'Search Results for: %s', 'sapalumni' ), '<span>' . esc_html( get_search_query() ) . '</span>' ); ?></h1>

			<div class="post-list">
			<?php
			// Start the loop.
			while ( have_posts() ) : the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );

			// End the loop.
			endwhile;


				// Previous/next page navigation.
				the_posts_pagination( array(
					'screen_reader_text' => __( 'Page:', 'sapalumni' ),
					'prev_text'          => __( '<', 'sapalumni' ),
					'next_text'          => __( '>', 'sapalumni' ),
					'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( '', 'sapalumni' ) . ' </span>',
				) );
			?>

			</div>
				<?php

		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</div>
		</div>
	</main>

<?php get_footer(); ?>
