<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage SAP_Alumni
 * @since SAP Alumni 1.0
 */

get_header(); ?>

<main class="main clearfix">
	<section class="error-404 not-found">
		<div class="p404-header">
			<h2>404</h2>
			<h4>Page not found</h4>
			<p>Unfortunately we can’t find the page you’re looking for. Sorry.</p>
		</div>
		<div class="container">
			<div class="p404-body">
				<p>Maybe try one of these instead:</p>
				<div class="row">
					<div class="col-md-4 p404-item">
						<div class="img-holder">
							<a href="/state-of-corporate-alumni-2017-survey-results/">
								<img src="<?=get_template_directory_uri()?>/images/404-info.png">
							</a>
						</div>
						<p>Read The State of Alumni 2017 Infographic</p>
					</div>
					<div class="col-md-4 p404-item">
						<div class="img-holder">
							<a href="/blog">
								<img src="<?=get_template_directory_uri()?>/images/404-blog.png">
							</a>
						</div>
						<p>View the latest blog pieces</p>
					</div>
					<div class="col-md-4 p404-item">
						<div class="img-holder">
							<a href="/">
								<img src="<?=get_template_directory_uri()?>/images/404-home.png">
							</a>
						</div>
						<p>Head back home</p>
					</div>
				</div>
			</div>
		</div>
	</section><!-- .error-404 -->
</main><!-- .site-main -->

<?php get_footer(); ?>
