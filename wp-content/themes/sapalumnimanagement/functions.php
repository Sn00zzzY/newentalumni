<?php
/**
 * SAP Alumni functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage SAP_Alumni
 * @since SAP Alumni 1.0
 */

/**
 * SAP Alumni only works in WordPress 4.4 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.4-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'sapalumni_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * Create your own sapalumni_setup() function to override in a child theme.
 *
 * @since SAP Alumni 1.0
 */
function sapalumni_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/sapalumni
	 * If you're building a theme based on SAP Alumni, use a find and replace
	 * to change 'sapalumni' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'sapalumni' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for custom logo.
	 *
	 *  @since SAP Alumni 1.2
	 */
	add_theme_support( 'custom-logo', array(
		'height'      => 240,
		'width'       => 240,
		'flex-height' => true,
	) );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 1200, 9999 );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'sapalumni' ),
		'social'  => __( 'Social Links Menu', 'sapalumni' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'status',
		'audio',
		'chat',
	) );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( 'css/editor-style.css', sapalumni_fonts_url() ) );

	// Indicate widget sidebars can use selective refresh in the Customizer.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif; // sapalumni_setup
add_action( 'after_setup_theme', 'sapalumni_setup' );

/**
 * Sets the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 *
 * @since SAP Alumni 1.0
 */
function sapalumni_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'sapalumni_content_width', 840 );
}
add_action( 'after_setup_theme', 'sapalumni_content_width', 0 );

/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 * @since SAP Alumni 1.0
 */
function sapalumni_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'sapalumni' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'sapalumni' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'sapalumni_widgets_init' );

if ( ! function_exists( 'sapalumni_fonts_url' ) ) :
/**
 * Register Google fonts for SAP Alumni.
 *
 * Create your own sapalumni_fonts_url() function to override in a child theme.
 *
 * @since SAP Alumni 1.0
 *
 * @return string Google fonts URL for the theme.
 */
function sapalumni_fonts_url() {
	$fonts_url = '';
	$fonts     = array();
	$subsets   = 'latin,latin-ext';

	/* translators: If there are characters in your language that are not supported by Merriweather, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Merriweather font: on or off', 'sapalumni' ) ) {
		$fonts[] = 'Merriweather:400,700,900,400italic,700italic,900italic';
	}

	/* translators: If there are characters in your language that are not supported by Montserrat, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Montserrat font: on or off', 'sapalumni' ) ) {
		$fonts[] = 'Montserrat:400,700';
	}

	/* translators: If there are characters in your language that are not supported by Inconsolata, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'sapalumni' ) ) {
		$fonts[] = 'Inconsolata:400';
	}

	if ( $fonts ) {
		$fonts_url = add_query_arg( array(
			'family' => urlencode( implode( '|', $fonts ) ),
			'subset' => urlencode( $subsets ),
		), 'https://fonts.googleapis.com/css' );
	}

	return $fonts_url;
}
endif;

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since SAP Alumni 1.0
 */
function sapalumni_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'sapalumni_javascript_detection', 0 );

/**
 * Enqueues scripts and styles.
 *
 * @since SAP Alumni 1.0
 */
function sapalumni_scripts() {
	// Theme stylesheet.
	wp_enqueue_style( 'sapalumni-main-style', get_template_directory_uri() . '/css/app.css', array(), '1.0.0');
	wp_enqueue_style( 'sapalumni-new-home-style', get_template_directory_uri() . '/css/new-style.css', array(), '1.0.0');
	// Bootstrap Select stylesheet.
	wp_enqueue_style( 'sapalumni-bootstrap-style', get_template_directory_uri() . '/css/bootstrap-select.min.css', array(), '1.0.0');

	// Theme javascript.
	wp_enqueue_script( 'sapalumni-main-script', get_template_directory_uri() . '/js/app.js', array( 'jquery' ), '20160816', true );

	//Nav Handle Javascript
	wp_enqueue_script( 'sapalumni-menu-modal-script', get_template_directory_uri() . '/js/menu-modal-handle.js', array( 'jquery' ), '20160816', true );

	// Bootstrap javascript.
	wp_enqueue_script( 'sapalumni-bootstrap-script', get_template_directory_uri() . '/js/bootstrap-select.min.js', array( 'jquery' ), '20160816', true );

	// Select Picker.
	wp_enqueue_script( 'sapalumni-select-picker', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '20160816', true );

	//BxSlider for New Home Page
	if (is_page_template('template-home.php') || is_page_template('template-test.php')) { 
		wp_enqueue_script( 'sapalumni-new-home-script', get_template_directory_uri() . '/js/jquery.bxslider.min.js', array( 'jquery' ), '20160816', true );
	}

	//Media Related JS file
	if (is_page_template('template-media.php')) { 
		wp_enqueue_script( 'sapalumni-new-media-script', get_template_directory_uri() . '/js/media-handle.js', array( 'jquery' ), '20170316', true );
	}

	wp_localize_script( 'sapalumni-script', 'screenReaderText', array(
		'expand'   => __( 'expand child menu', 'sapalumni' ),
		'collapse' => __( 'collapse child menu', 'sapalumni' ),
	) );
}
add_action( 'wp_enqueue_scripts', 'sapalumni_scripts' );

/**
 * Adds custom classes to the array of body classes.
 *
 * @since SAP Alumni 1.0
 *
 * @param array $classes Classes for the body element.
 * @return array (Maybe) filtered body classes.
 */
function sapalumni_body_classes( $classes ) {
	// Adds a class of custom-background-image to sites with a custom background image.
	if ( get_background_image() ) {
		$classes[] = 'custom-background-image';
	}

	// Adds a class of group-blog to sites with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of no-sidebar to sites without active sidebar.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'sapalumni_body_classes' );

/**
 * Converts a HEX value to RGB.
 *
 * @since SAP Alumni 1.0
 *
 * @param string $color The original color, in 3- or 6-digit hexadecimal form.
 * @return array Array containing RGB (red, green, and blue) values for the given
 *               HEX code, empty array otherwise.
 */
function sapalumni_hex2rgb( $color ) {
	$color = trim( $color, '#' );

	if ( strlen( $color ) === 3 ) {
		$r = hexdec( substr( $color, 0, 1 ).substr( $color, 0, 1 ) );
		$g = hexdec( substr( $color, 1, 1 ).substr( $color, 1, 1 ) );
		$b = hexdec( substr( $color, 2, 1 ).substr( $color, 2, 1 ) );
	} else if ( strlen( $color ) === 6 ) {
		$r = hexdec( substr( $color, 0, 2 ) );
		$g = hexdec( substr( $color, 2, 2 ) );
		$b = hexdec( substr( $color, 4, 2 ) );
	} else {
		return array();
	}

	return array( 'red' => $r, 'green' => $g, 'blue' => $b );
}

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images
 *
 * @since SAP Alumni 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function sapalumni_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	840 <= $width && $sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px';

	if ( 'page' === get_post_type() ) {
		840 > $width && $sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
	} else {
		840 > $width && 600 <= $width && $sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px';
		600 > $width && $sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'sapalumni_content_image_sizes_attr', 10 , 2 );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails
 *
 * @since SAP Alumni 1.0
 *
 * @param array $attr Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size Registered image size or flat array of height and width dimensions.
 * @return string A source size value for use in a post thumbnail 'sizes' attribute.
 */
function sapalumni_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( 'post-thumbnail' === $size ) {
		is_active_sidebar( 'sidebar-1' ) && $attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 60vw, (max-width: 1362px) 62vw, 840px';
		! is_active_sidebar( 'sidebar-1' ) && $attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 88vw, 1200px';
	}
	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'sapalumni_post_thumbnail_sizes_attr', 10 , 3 );

/**
 * Modifies tag cloud widget arguments to have all tags in the widget same font size.
 *
 * @since SAP Alumni 1.1
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array A new modified arguments.
 */
function sapalumni_widget_tag_cloud_args( $args ) {
	$args['largest'] = 1;
	$args['smallest'] = 1;
	$args['unit'] = 'em';
	return $args;
}
add_filter( 'widget_tag_cloud_args', 'sapalumni_widget_tag_cloud_args' );
//
//
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}
@ini_set( 'upload_max_size' , '256M' );
@ini_set( 'post_max_size', '256M');
@ini_set( 'max_execution_time', '3000' );

include_once('components/breadcrumbs.php');
include_once('components/category_extra_field.php');

add_filter('widget_posts_args', 'filterPostFromWidget');

/**
 * Filter data from recent posts widjet
 * @param array $args  An array of default widget arguments.
 * @return array
 */
function filterPostFromWidget($args) {
	$args['category__not_in'] = 1;

	return $args;
}

add_action('init', 'initApiRoutes');

/**
 * Init routes for api
 */
function initApiRoutes()
{
	add_action('rest_api_init', 'registerRoutes');
}

function registerRoutes()
{
	register_rest_route('v1', '/get-last-blog-post', [
		'methods' => 'POST',
		'callback' => 'getLastBlogPost'
	]);
}

/**
 * Get last blog post for rest api widget
 * @return string|null
 */
function getLastBlogPost()
{
	$conf = [
		'hash' => '4df5198b639817f3863bdb1a831e942d',
	];

	$result = [];

	if (!empty($_POST['hash'] && $_POST['hash'] !== $conf['hash'])) {
		return;
	}

	$args = [
		'numberposts' => '3',
		'post_status' => 'publish',
		'category__not_in' => 1,
	];

	$recent_posts = wp_get_recent_posts($args);

	if (!$recent_posts) {
		return;
	}

	foreach($recent_posts as $recent) {
		$current = [];

		$current['dateJ'] = get_the_date("j", $recent["ID"]);
		$current['dateM'] = get_the_date("M", $recent["ID"]);
		$current['postLink'] = get_permalink($recent["ID"]);
		$current['postTitle'] = $recent["post_title"];

		$author_name = get_userdata($recent["post_author"])->display_name;
		$current['authorName'] = $author_name;
		$current['authorLink'] = get_site_url() . '/author/' . $author_name;
		$current['blogLink'] = get_site_url() . '/blog';

		$categories = get_the_category($recent["ID"]);

		if(count($categories) >= 1) {
			foreach ($categories as $category) {
				$cat = [];

				if ($category->term_id != 1) {
					$cat['name'] = $category->name;
					$cat['slug'] = get_site_url() . '/category/' . $category->slug;
				}

				if ($cat) {
					$current['category'][] = $cat;
				}
			}
		}
		$result[] = $current;
	}

	echo json_encode($result); die;
}
/*
* Creating a function to create our CPT
*/

function custom_post_type() {

// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => __( 'Tickets', 'Post Type General Name' ),
        'singular_name'       => __( 'Ticket', 'Post Type Singular Name'),
        'menu_name'           => __( 'Tickets' ),
        'parent_item_colon'   => __( 'Parent Ticket'),
        'all_items'           => __( 'All Tickets'),
        'view_item'           => __( 'View Ticket'),
        'add_new_item'        => __( 'Add New Ticket'),
        'add_new'             => __( 'Add New'),
        'edit_item'           => __( 'Edit Ticket'),
        'update_item'         => __( 'Update Ticket'),
        'search_items'        => __( 'Search Tickets'),
        'not_found'           => __( 'Not Found'),
        'not_found_in_trash'  => __( 'Not found in Trash'),
    );

// Set other options for Custom Post Type

    $args = array(
        'label'               => __( 'ticket'),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'thumbnail'),
        // You can associate this CPT with a taxonomy or custom taxonomy.
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );

    // Registering your Custom Post Type
    register_post_type( 'tickets', $args );

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/

add_action( 'init', 'custom_post_type', 0 );
