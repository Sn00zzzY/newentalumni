jQuery("document").ready(function() {
    jQuery(".menu-item-contact").on('click', function() {
        jQuery(".main-body").show();
        jQuery(".thank-body").hide();
        jQuery("#contactModal").modal('show');
    });

    jQuery(".trigger-alright").on('click', function() {
        jQuery(".main-body").show();
        jQuery(".thank-body").hide();
        jQuery("#alrightModal").modal('show');
    })

    jQuery(".menu-toggle").on("click", function() {
        jQuery(".menu-right").toggleClass("menu-open");
    });

    jQuery("select.header-drop").on("change", function() {
        if($(this).val() == "Enterprise Edition") {
            document.location.href = "/";
        } else {
            document.location.href = "/alumni-management-small-medium-business/";
        }
    });

    jQuery(document).keyup(function(e) {
        if (e.keyCode === 27) {
            jQuery("#alrightModal").modal('hide');
            jQuery("#contactModal").modal('hide');
        }
    });

    //in case of home page
    if(document.location.pathname === '/' || document.location.pathname === '/test-page/') {
        var interval = jQuery(".bxslider").data("bxpause");
        if(!interval) interval = 3500;
        jQuery(".bxslider").bxSlider({
            mode: 'fade',
            auto: true,
            pause: interval
        });
    }
});

jQuery(window).load(function() {
    if(document.location.pathname !== '/' && document.location.pathname !== "/alumni-management-small-medium-business/") {
        $(".selectpicker").selectpicker();
        jQuery("select.header-drop").val(" ");
    }
})