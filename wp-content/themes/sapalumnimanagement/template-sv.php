<?php
/**
 * Template Name: SV Template
 */
get_header(); ?>


<main class="main">
  <?php get_template_part( 'template-parts/hero' ); ?>
  <?php get_template_part( 'template-parts/sv-info' ); ?>
  <?php get_template_part( 'template-parts/sv-contact' ); ?>
</main>

<?php get_footer(); ?>