<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage SAP_Alumni
 * @since SAP Alumni 1.0
 */

?><!DOCTYPE html>
<?php $base_path = esc_url( get_template_directory_uri() ); ?>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<link rel="apple-touch-icon" type="image/png" sizes="57x57" href="<?php echo $base_path; ?>/favicons/57.png">
	<link rel="apple-touch-icon" type="image/png" sizes="60x60" href="<?php echo $base_path; ?>/favicons/60.png">
	<link rel="apple-touch-icon" type="image/png" sizes="72x72" href="<?php echo $base_path; ?>/favicons/72.png">
	<link rel="apple-touch-icon" type="image/png" sizes="76x76" href="<?php echo $base_path; ?>/favicons/76.png">
	<link rel="apple-touch-icon" type="image/png" sizes="114x114" href="<?php echo $base_path; ?>/favicons/114.png">
	<link rel="apple-touch-icon" type="image/png" sizes="120x120" href="<?php echo $base_path; ?>/favicons/120.png">
	<link rel="apple-touch-icon" type="image/png" sizes="144x144" href="<?php echo $base_path; ?>/favicons/144.png">
	<link rel="apple-touch-icon" type="image/png" sizes="152x152" href="<?php echo $base_path; ?>/favicons/152.png">
	<link rel="apple-touch-icon" type="image/png" sizes="180x180" href="<?php echo $base_path; ?>/favicons/180.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $base_path; ?>/favicons/16.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $base_path; ?>/favicons/32.png">
	<link rel="icon" type="image/png" sizes="48x48" href="<?php echo $base_path; ?>/favicons/48.png">
	<link rel="icon" type="image/png" sizes="64x64" href="<?php echo $base_path; ?>/favicons/64.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo $base_path; ?>/favicons/96.png">
	<link rel="icon" type="image/png" sizes="128x128" href="<?php echo $base_path; ?>/favicons/128.png">
	<link rel="icon" type="image/png" sizes="192x192" href="<?php echo $base_path; ?>/favicons/192.png">
	<link rel="icon" type="image/png" sizes="194x194" href="<?php echo $base_path; ?>/favicons/194.png">
	<link rel="icon" type="image/png" sizes="195x195" href="<?php echo $base_path; ?>/favicons/195.png">
	<?php wp_head(); ?>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<meta charset="<?php bloginfo( 'charset' ); ?>">
</head>

<body <?php body_class(); ?>>

<!--<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-5038135-23', 'auto');
	ga('send', 'pageview');

</script>-->

<div class="layout">
	<header class="header">
		<div class="container">
            <div class="menu-bar clearfix">
                <div class="menu-left">
                    <a href="/" class="logo">
                        <img src="<?php echo get_field('logo', option); ?>" alt="logo">
                    </a>
                </div>
                <div class="menu-right">
                    <?php if ( has_nav_menu( 'primary' ) ) : ?>
                        <?php
							wp_nav_menu( array(
								'theme_location' => 'primary',
								'menu_class'     => 'menu top-menu',
								'container'		 => false
							) );
						?>
					<?php endif; ?>
                    <!-- <select class="selectpicker header-drop">
                        <option <?php if(get_page_template_slug() === 'template-newhome.php') echo ' selected'; ?>>SMB Edition</option>
                        <option <?php if(get_page_template_slug() !== 'template-newhome.php') echo ' selected'; ?>>Enterprise Edition</option>
                    </select> -->
                    <a href="<?php echo get_field('button_link', option); ?>" class="btn btn-big btn-default"><?php echo get_field('button_text', option); ?></a>
                </div>
                <div class="menu-toggle">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
		<!-- Contact Modal -->
        <div id="contactModal" class="modal fade custom-modal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-body main-body">
                    	<div class="btn-modal-close" data-dismiss="modal"></div>
                        <p class="title">Say “hello!”</p>
                        <p class="description">Drop us a message if you would like to know more about Alumni Management and how it can benefit your organization.</p>
                        <?php echo do_shortcode("[contact-form-7 id=\"633\" title=\"ALUMNI Contact(Modal)\"]"); ?>
                    </div>
                    <div class="modal-body thank-body">
                        <p class="title">Hey, thanks!</p>
                        <p class="description">We really appreciate your interest and will get back to you shortly.</p>
                        <div class="row">
                            <div class="col-sm-12">
                                <button class="btn btn-custom btn-blue with-border" type="button" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</header>