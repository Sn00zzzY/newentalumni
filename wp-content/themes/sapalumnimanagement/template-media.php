<?php
/**
 * Template Name: Media Download Template
 */
get_header(); ?>

	<section class="section-download">
        <div class="content">
            <div class="content-title">
                <h3 class="title">Media Downloads</h3>
                <p class="subtitle">Login to access our library of EnterpriseAlumni images, media and promotional downloads.</p>
            </div>
            <div class="login-form">
                <form class="form">
                    <div class="form-group">
                        <input type="email" autocomplete="off" class="form-control" placeholder="Enter your email">
                    </div>
                    <div class="form-group">
                        <input type="password" autocomplete="off" class="form-control" placeholder="Enter your password">
                    </div>
                    <input style="display:none;" type="email" name="fakeusernameremembered">
                    <input style="display:none;" type="password" name="fakeuserpasswordremembered">
                    <button type="button" class="btn btn-primary btn-submit">Log in</button>
                </form>
                <div class="error-notification"><b>Whoops!</b> we do not recognise that email/password combination. please try requesting access below</div>
            </div>
            <div class="request-block">
                <p class="subtitle">First time visitor and require a password?</p>
                <button type="button" class="btn btn-primary btn-line">Request Access</button>
            </div>

            <div class="request-form">
                <h4 class="title">Request Access</h4>
                <button class="close" type="button"></button>
                <form class="form">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Enter first name">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Enter last name">
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="Enter your email">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Enter your company">
                    </div>
                    <button type="button" class="btn btn-primary btn-submit">Send Request</button>
                </form>
            </div>

            <div class="thankyou-form">
                <img src="<?php echo get_template_directory_uri(); ?>/images/icon-check-green.png">
                <p class="subtitle"><b>Thank you!</b> Your request has been received. We will contact you shortly to grant access.</p>
                <a href="#" class="btn btn-primary btn-goback">Back to homepage</a>
                <p class="notification">You will be-directed back to the home page in <span>5</span> sec</p>
            </div>
        </div>
    </section>

<?php get_footer(); ?>