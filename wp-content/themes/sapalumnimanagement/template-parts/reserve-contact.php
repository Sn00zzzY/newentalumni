<section class="section contacts-section" id="contact">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="form-holder">
          <p><?php echo get_field('contact_content'); ?></p>
          <div class="contact-form">
            <?php echo do_shortcode('[contact-form-7 id="190" title="Contact form (Reserve)"]'); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>