<section class="section about-section" id="overview">
	<div class="container">
	    <div class="row">
			<div class="col-md-8">
				<div class="content">
					<strong class="title"><?php echo get_field('section_title'); ?><span><?php echo get_field('section_subtitle'); ?></span></strong>
					<div class="main-text"><?php echo get_field('content_title'); ?></div>
			    	<?php echo get_field('content_text'); ?>
			    	<div class="about-why">
			    		<h3><?php echo get_field('new_about_bottom'); ?></h3>
			    		<div class="why-inner">
			    			<div class="row">
				    		<?php if( have_rows('new_about_bottom_list') ): ?>
	          					<?php while ( have_rows('new_about_bottom_list') ) : the_row(); ?>
	          						<div class="col-sm-6 about-why-item">
		          						<p><?php echo get_sub_field('about_bottom_list_text'); ?></p>
										<div class="img-holder">
											<img src="<?php echo get_sub_field('about_bottom_list_image'); ?>" alt="Enterpise Logo">
										</div>
									</div>
								<?php endwhile; ?>
							<?php endif; ?>
							</div>
						</div>
			    	</div>
			    </div>
			</div>
			<div class="col-md-4">
				<div class="sidebar">
					<div class="wp-video">
						<video class="video-shortcode" width="100%" loop="1" preload="metadata" controls="controls" poster="<?php echo get_field("video_poster"); ?>">
							<source type="video/mp4" src="<?php echo get_field("video");?>">
						</video>
					</div>
					<div class="post-holder">
						<h2>From the Blog</h2>			
						<div class="post-list">
						<?php $args = array(
							'numberposts' => '4',
							'post_status' => 'publish',
							'category__not_in' => 1,
						 ); ?>
						<?php $recent_posts = wp_get_recent_posts( $args ); ?>
						
							<?php foreach( $recent_posts as $recent ){ ?>
								<?php $categories = get_the_category($recent["ID"]); ?> 

								<article class="post">
									<time class="post-date">
										<?php echo get_the_date( "j", $recent["ID"] ); ?>
										<span><?php echo get_the_date( "M", $recent["ID"] ); ?> </span>
									</time>

									<h3 class="post-title">
										<a href="<?php echo get_permalink($recent["ID"]); ?>">
											<?php echo $recent["post_title"]; ?>
										</a>
									</h3>

									<div class="post-meta">by 
										<?php $author_name = get_userdata($recent["post_author"])->display_name

										?>
										<a href="<?php echo get_author_posts_url( $recent["post_author"] ); ?>
" class="post-author">
										<?php echo $author_name; ?></a>
										<?php if( count($categories) >= 1 ) : ?>

											<ul class="post-categories"> 
											
											&nbsp;in 

												<?php foreach ( $categories as $category ){ ?>
													<?php if ( ($category->term_id) != 1 ) : ?>
														<?php $cat_name = $category->name; ?>
														<?php $cat_slug = $category->slug; ?>
														<li>
															<a href="/category/<?php echo $cat_slug; ?>" rel="category tag">
																&nbsp;<?php echo $cat_name; ?>
															</a>
														</li>
													<?php endif; ?>
												<?php } ?>

											</ul>

										<?php endif; ?>

									</div>
								</article>

							<?php } ?>
						</div>
						<div class="links-holder clearfix">
							<a href="/blog" class="link-go pull-right">Go to the Blog <i class="fa fa-angle-right"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
</section>