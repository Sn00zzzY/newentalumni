<div class="adv-section">
	<div class="container">
		<div class="adv-items">
			<div class="adv-item">
				<div class="ent-item">
					<div class="item-left">
						<div class="img-holder">
							<img src="<?=get_template_directory_uri();?>/images/ea-logo-black.png">
						</div>
						<p>Are you a brand looking to sell into 1.5m+ corporate professionals?</p>
					</div>
					<div class="item-right">
						<div class="ipad-holder">
							<img src="<?=get_template_directory_uri();?>/images/adv-market-ipad.png">
						</div>
					</div>
					<div class="link-holder">
						<a href="http://www.alumni-marketplace.com/" class="market-link">Visit Alumni Marketplace<i class="fa fa-angle-right"></i></a>
					</div>
				</div>
			</div>
			<div class="adv-item">
				<div class="gig-item">
					<div class="img-holder">
						<img src="<?=get_template_directory_uri();?>/images/adv-gig-logo.png">
					</div>
					<p>Enterprise looking for On Demand Contingent Talent? Access AlumniGigs - post a job and define eligibility criteria.</p>
					<div class="link-holder">
						<a href="http://alumnigigs.com/" class="gig-link">VISIT ALUMNI GIGS<i class="fa fa-angle-right"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>