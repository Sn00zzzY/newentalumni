<?php
/**
 * The template part for displaying results in search pages
 *
 * @package WordPress
 * @subpackage SAP_Alumni
 * @since SAP Alumni 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<time class="post-date"><?php echo get_the_date('d'); ?> <span><?php echo get_the_date('M'); ?></span></time>
	<h3 class="post-title"><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h3>
	<div class="post-meta">by <span class="post-author"><?php echo get_the_author_link(); ?></span> in <?php the_category(); ?></div>
		<div class="post-image">
			<?php the_post_thumbnail(); ?>
		</div>
	<div class="post-text">
		<?php //the_excerpt(); ?>
		<?php echo wp_trim_words( get_the_content(), 40, ' [...]' ); ?>
	</div>
	<a href="<?php the_permalink();?>" class="btn btn-view post-link">View Full Post</a>
</article>

