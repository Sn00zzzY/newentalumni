<section class="section business-section text-center" id="business">
  <div class="container">
    <strong class="title"><?php echo get_field('business_section_title'); ?></strong>
    <div class="block-business">
      <ul class="list-business">

        <?php if( have_rows('outcomes') ): ?>

          <?php while ( have_rows('outcomes') ) : the_row(); ?>

            <li>
              <div class="icon">
                <img src="<?php echo get_sub_field('item_icon'); ?>" alt="intelligence icon">
              </div>
              <div class="description"><?php echo get_sub_field('item_text'); ?></div>
            </li>

          <?php endwhile; ?>

        <?php else : ?>
        <!--// no rows found-->
        <?php endif; ?>

      </ul>
    </div>
  </div>
</section>