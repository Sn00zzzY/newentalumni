<div class="p-features">
    <div class="container">
        <h3 class="p-title"><?php echo get_field('new_p_feature_title'); ?></h3>
        <div class="row">
            <div class="col-md-4">
                <ul class="p-feature">
                    <?php if( have_rows('new_power_feature_list') ): ?>
                        <?php while ( have_rows('new_power_feature_list') ) : the_row(); ?>
                            <li><span><?php echo get_sub_field('new_power_feature_item'); ?></span></li>
                        <?php endwhile; ?>
                    <?php endif; ?>    
                </ul>
                <p class="p-hint">+ Many more powerful features!</p>
                <a href="<?php echo get_field('new_power_feature_button_link'); ?>" class="btn btn-custom btn-yellow trigger-alright">Begin FREE 30 Day Trial</a>
            </div>
            <div class="col-md-7 col-md-offset-1">
                <div class="ipad-holder">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/ipad-frame.png" alt="frame">
                    <div class="ipad-inside">
                        <div class="bxslider">
                            <?php if( have_rows('new_power_feature_screens') ): ?>
                                <?php while ( have_rows('new_power_feature_screens') ) : the_row(); ?>
                                    <li>
                                        <img src="<?php echo get_sub_field('new_power_feature_image')['url']; ?>" alt="bx image">
                                        <span><?php echo get_sub_field('new_power_feature_desc'); ?></span>
                                    </li>
                                <?php endwhile; ?>
                            <?php endif; ?>   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
