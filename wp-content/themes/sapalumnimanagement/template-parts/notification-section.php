<section class="section notificaton-section" id="notificaton">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="notification-service-holder">
                    <h3>Service Notifications</h3>
                    <?php if (have_rows('services_repeater')): ?>
                        <ul class="notification-service-list">
                            <?php while (have_rows('services_repeater')) : the_row(); ?>
                                <li>
                                    <?php
                                    $status = get_sub_field('service_status');
                                    ?>
                                    <span
                                        class="status <?php if ($status === 'not_working'): ?>unsuccess<?php endif; ?>"><?php if ($status === 'working'): ?>
                                            <i class="fa fa-check"
                                               aria-hidden="true"></i> <?php else: ?>
                                            <i class="fa fa-times" aria-hidden="true"></i> <?php endif; ?></span>
                                    <div class="description">
                                        <h4><?php the_sub_field('service_name'); ?></h4>
                                        <?php if ($status === 'working'): ?>
                                            Service is operating normally
                                        <?php else: ?>
                                            <?php the_sub_field('not_working_reason'); ?>
                                        <?php endif; ?>
                                    </div>
                                </li>

                            <?php endwhile; ?>
                        </ul>
                    <?php endif; ?>
                </div>
                <div class="block-scheduled">
                    <h3>Scheduled Downtime</h3>
                    <?php if (get_field('sheduled_downtime') != NUll or get_field('sheduled_downtime') != '' or get_field('sheduled_downtime')) : ?>
                        <?php the_field('sheduled_downtime'); ?>
                    <?php else: ?>
                        There is currently no upcoming
                        scheduled downtime
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-8 col-sm-6">
                <div class="notification-holder">
                    <h2>Notifications</h2>
                    <div class="info">If you are experiencing an issue not reflected here please <a href="#">contact
                            support</a>.
                    </div>
                    <?php
                    $args = array('post_type' => 'tickets', 'posts_per_page' => 7);
                    $loop = new WP_Query($args);
                    while ($loop->have_posts()) :
                        $loop->the_post(); ?>
                        <div class="notification-block">
                            <div class="title-notification"><?php echo get_the_date('F d Y, T H:i'); ?>
                                <span class="ticket"><?php the_title(); ?></span>
                            </div>
                            <p><?php echo strip_tags(get_the_content());?></p>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </div>
</section>