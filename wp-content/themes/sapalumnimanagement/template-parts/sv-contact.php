<section class="section interested-section">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="form-holder">
          <strong class="title">Interested?</strong>
          <p>Drop us your details and we will respond with more information.</p>
          <div class="contact-form">
            <?php echo do_shortcode('[contact-form-7 id="190" title="Contact form (Reserve)"]'); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>