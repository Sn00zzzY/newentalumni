<section class="section contacts-section" id="contact">
  <div class="container">
    <div class="row">
      <div class="col-md-5">
        <strond class="title"><?php echo get_field('message_title');  ?></strond>
        <blockquote>
          <q><?php echo get_field('message_text'); ?></q>
          <i><?php echo get_field('message_sign'); ?></i>
          <div class="autor-holder">
            <div class="img-holder">
              <img src="<?php echo get_field('message_sender_avatar');  ?>" alt="user">
            </div>
            <cite><strong><?php echo get_field('message_author_name');  ?></strong> <?php echo get_field('message_author_position');  ?></cite>
          </div>
        </blockquote>
      </div>
      <div class="col-md-5 col-md-offset-1">
        <div class="form-holder">
          <p>For more information, to schedule a demo or gain access to our info sheets and other materials contact us:</p>
          <div class="contact-form">
            <?php echo do_shortcode('[contact-form-7 id="48" title="Contact form (Home)"]'); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>