<section class="section promo-section promo-reserve" id="home">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="title-holder">
          <img src="<?php echo get_field('event_image'); ?>" alt="success connect logo" class="img-responsive">
          <h1><?php echo get_field('event_place'); ?> <time><?php echo get_field('event_date'); ?></time></h1>
        </div>
        <div class="form-holder reserve-form">
          <p><?php echo get_field('contact_content'); ?></p>
          <div class="contact-form">
            <?php echo do_shortcode('[contact-form-7 id="190" title="Contact form (Reserve)"]'); ?>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="onesheet-holder">
          <div class="text-holder text-center">
            <p><?php echo get_field('document_info'); ?></p>
          </div>
          <div class="img-holder">
            <img src="<?php echo get_field('document_image'); ?>" alt="">
          </div>
        </div>
      </div>
    </div>
  </div>
</section>