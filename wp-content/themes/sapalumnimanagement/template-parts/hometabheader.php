<div class="home-tabs">
    <div class="container">
        <ul class="nav nav-tabs">
            <li class="active">
                <a data-toggle="tab" href="#large-org"><?php echo get_field('large_org_title'); ?><span><?php echo get_field('large_org_bound'); ?></span></a>
            </li>
            <li>
                <a data-toggle="tab" href="#small-org"><?php echo get_field('sbe_org_title'); ?><span><?php echo get_field('sbe_org_bound'); ?></span></a>
            </li>
        </ul>
    </div>
</div>