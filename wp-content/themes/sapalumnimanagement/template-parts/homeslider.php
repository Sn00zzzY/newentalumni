<div class="hero-home-slider promo-section">
    <div class="container">
        <div class="bxslider" data-bxpause="7000">
            <?php if( have_rows('slider_list') ): ?>
                <?php while ( have_rows('slider_list') ) : the_row(); ?>
                    <li>
                        <div class="col-sm-5 home-slider-text">
                            <?php echo get_sub_field('home_slider_content'); ?>
                        </div>
                        <div class="col-sm-7">
                            <div class="img-holder">
                                <img src="<?php echo get_sub_field('home_slide_image'); ?>" alt="bx image"> 
                            </div>
                        </div>                        
                    </li>
                <?php endwhile; ?>
            <?php endif; ?>   
        </div>
    </div>
</div>