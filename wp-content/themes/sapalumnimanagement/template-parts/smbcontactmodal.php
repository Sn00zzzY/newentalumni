<div id="alrightModal" class="modal fade custom-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body main-body">
                <p class="title">Alright!</p>
                <p class="description">We are in beta with our first wave of customers, please fill out the following details and we will get you set up ASAP  :)</p>
                <?php echo do_shortcode("[contact-form-7 id=\"632\" title=\"ALUMNI SMB Contact(Modal)\"]"); ?>
            </div>
            <div class="modal-body thank-body">
                <p class="title">Hey, thanks!</p>
                <p class="description">We really appreciate your interest and will get back to you shortly.</p>
                <div class="row">
                    <div class="col-sm-12">
                        <button class="btn btn-custom btn-blue with-border" type="button" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>