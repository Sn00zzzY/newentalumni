<div class="feature-bar">
    <div class="container">
        <h3 class="feature-title"><?php echo get_field('new_feature_title'); ?></h3>
        <p class="feature-desc"><?php echo get_field('new_feature_description'); ?></p>
        <div class="row">
            <?php if( have_rows('new_feature_list') ): ?>
                <?php while ( have_rows('new_feature_list') ) : the_row(); ?>
                    <div class="col-md-4">
                        <div class="feature">
                            <div class="img-holder">
                                <img src="<?php echo get_sub_field('new_feature_list_icon')['url']; ?>" alt="feature list icon">
                            </div>
                            <p class="subtitle"><?php echo get_sub_field('new_feature_list_title'); ?></p>
                            <span><?php echo get_sub_field('new_feature_list_text'); ?></span>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>    
        </div>
    </div>
</div>
