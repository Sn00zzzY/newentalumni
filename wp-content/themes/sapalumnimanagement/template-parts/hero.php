<section class="section promo-section" id="home">
  <div class="container">
    <div class="row">
      <div class="col-sm-5">
        <div class="title-holder">
          <?php echo get_field('hero_content'); ?>
        </div>
      </div>
      <div class="col-lg-7 col-md-7 col-sm-7">
        <?php if(get_page_template_slug() === 'template-home.php'): ?>
          <div class="img-holder">
        <?php else: ?>
          <div class="img-holder2">
        <?php endif; ?>
          <img src="<?php echo get_field('hero_image'); ?>" alt="hero image" class="img-responsive">
        </div>
      </div>
    </div>
  </div>
</section>