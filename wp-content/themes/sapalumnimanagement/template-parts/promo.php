<section class="section promo-section" id="home">
  <div class="container">
    <div class="row">
      <div class="col-md-5">
        <div class="title-holder">
          <img src="<?php echo get_field('event_image'); ?>" alt="success connect logo" class="img-responsive">
          <h1><?php echo get_field('event_place'); ?> <time><?php echo get_field('event_date'); ?></time></h1>
        </div>
      </div>

      <div class="col-md-7">
        <div class="img-holder">
          <img src="<?php echo get_field('document_image'); ?>" alt="">
          <div class="text-holder text-center">
            <p><?php echo get_field('document_info'); ?></p>
            <a href="<?php echo get_field('document_link'); ?>" target="_blank" class="btn btn-primary">Download</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>