<section class="section section-modules">
  <div class="container">
    <div class="first-slider clearfix">
      <div class="text">
        <ul>

          <?php if( have_rows('modules_slider') ): ?>
          <?php $i = 0; ?>
          <?php while ( have_rows('modules_slider') ) : the_row(); ?>


          <li data-slide="<?php echo $i; ?>">
            <div class="slide">
              <div class="row">
                <div class="col-md-7 col-md-push-5">
                  <img src="<?php echo get_sub_field('slide_image'); ?>" alt="slide image">
                </div>
                <div class="col-md-5 col-md-pull-7">
                  <div class="visible-lg">
                    <img src="<?php echo get_sub_field('slide_icon'); ?>" alt="slide icon">
                  </div>
                  <h4><?php echo get_sub_field('slide_title'); ?></h4>
                  <p><?php echo get_sub_field('slide_text'); ?></p>
                </div>
              </div>
            </div>
          </li>

          <?php $i++; ?>

          <?php endwhile; ?>

          <?php else : ?>
          <!--// no rows found-->
          <?php endif; ?>

        </ul>
      </div>
    </div>

    <div class="second-slider">
      <ul>

        <?php if( have_rows('modules_slider') ): ?>
        <?php $j = 0; ?>
        <?php while ( have_rows('modules_slider') ) : the_row(); ?>
        <li data-slide="<?php echo $j; ?>">
          <a href="javascript:void(0);" class="<?php if($j == 0) echo 'active'; ?>" >
            <img src="<?php echo get_sub_field('slide_navigation_icon_normal'); ?>" alt="dashboard" class="active-img">
            <img src="<?php echo get_sub_field('slide_navigation_icon_hover'); ?>" alt="dashboard" class="inactive-img">
          </a>
        </li>
        <?php $j++; ?>
        <?php endwhile; ?>
        <?php else : ?>
        <!--// no rows found-->
        <?php endif; ?>

      </ul>
    </div>
  </div>
</section>