<section class="section service-section text-center" id="platform">
  <div class="container">
    <strong class="title"><?php echo get_field('services_section_title'); ?></strong>
    <p><?php echo get_field('services_section_content_start'); ?></p>
    <div class="block-service visible-xs">
      <div class="tags-holder">

        <?php if( have_rows('tag_list') ): ?>

        <?php $i = 0; ?>

        <?php while ( have_rows('tag_list') ) : the_row(); ?>

          <?php if ($i == 0): ?>
            <div>
              <span class="tag"><?php echo get_sub_field('tag_title'); ?></span>
            </div>
          <?php endif; ?>
          <span class="tag"><?php echo get_sub_field('tag_title'); ?></span>

          <?php $i++; ?>
        <?php endwhile; ?>

        <?php else : ?>
        <!--// no rows found-->
        <?php endif; ?>

      </div>
      <img src="<?php echo get_field('mobile_image'); ?>" alt="sap" class="img-responsive">
    </div>
    <div class="img-holder">
      <img src="<?php echo get_field('desktop_main_image'); ?>" alt="service" class="hidden-xs">
      <img src="<?php echo get_field('desktop_side_image'); ?>" alt="sap">
    </div>
    <p><br><br><br><?php echo get_field('services_section_content_end'); ?></p>
  </div>
</section>