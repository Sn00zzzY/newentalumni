<div class="contact-bar">
    <div class="container">
        <div class="two-col">
            <div class="col-content">
                <div class="content">
                    <h4><?php echo get_field('new_contact_title'); ?></h4>
                    <p><?php echo get_field('new_contact_description'); ?></p>
                </div>
            </div>
            <div class="col-content">
                <div class="content">
                    <div class="contact-form">
                        <?php echo do_shortcode(get_field('new_contact_form_shortcode')); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
