<section class="section integration-section text-center" id="integrations">
  <div class="container">
    <strong class="title"><?php echo get_field('integration_section_title'); ?></strong>
    <p><?php echo get_field('integration_section_content'); ?></p>
    <ul class="list-logo">

      <?php if( have_rows('companies_list') ): ?>
        <?php while ( have_rows('companies_list') ) : the_row(); ?>

          <li>
            <img src="<?php echo get_sub_field('company_logo_normal'); ?>" alt="logo normal" class="img">
            <img src="<?php echo get_sub_field('company_logo_hovered'); ?>" alt="logo hover" class="img-hover">
          </li>

        <?php endwhile; ?>
      <?php else : ?>
      <!--// no rows found-->
      <?php endif; ?>

    </ul>
    <span class="add">And many more</span>
  </div>
</section>