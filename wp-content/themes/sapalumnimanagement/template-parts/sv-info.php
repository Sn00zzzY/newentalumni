<section class="section info-section">
  <div class="container">
    <strong class="title"><?php echo get_field('info_title'); ?></strong>
   <?php echo get_field('info_content'); ?>
    <div class="row holder">
      <div class="col-md-6">
        <?php echo get_field('info_content_left'); ?>
      </div>
      <div class="col-md-5 col-md-offset-1">
        <div class="onesheet-holder">
          <div class="text-holder text-center">
            <p><?php echo get_field('onesheet_image_title'); ?></p>
          </div>
          <div class="img-holder">
            <img src="<?php echo get_field('info_image'); ?>" alt="" class="img-responsive">
          </div>
        </div>
      </div>
    </div>
  </div>
</section>