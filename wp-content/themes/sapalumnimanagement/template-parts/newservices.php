<div class="service-bar" style="background-image: url(<?php echo get_field('new_service_background')['url']; ?>);">
    <div class="shadow-box">
        <div class="container">
            <h4><?php echo get_field('new_service_title'); ?></h4>
            <div class="row">
                <?php if( have_rows('new_service_list') ): ?>
                    <?php 
                        $svc_count = 0;
                        while ( have_rows('new_service_list') ) : the_row(); ?>
                        <div class="col-md-4">
                            <div class="service service-<?php echo get_sub_field('new_service_item_color'); ?>">
                                <p class="service-subtitle"><?php echo get_sub_field('new_service_item_title'); ?></p>
                                <p><?php echo get_sub_field('new_service_item_type'); ?></p>
                                <div class="content">
                                    <span class="service-label"><?php echo get_sub_field('new_service_item_upper'); ?></span>
                                    <span class="service-cap"><?php echo get_sub_field('new_service_item_capacity'); ?></span>
                                    <span class="service-label">Alumni</span>
                                    <span><?php echo get_sub_field('new_service_item_description'); ?></span>
                                    <a href="<?php echo get_sub_field('new_service_item_button_link'); ?>" 
                                       class="btn btn-<?php echo get_sub_field('new_service_item_color'); ?> btn-custom btn-sm btn <?php if(++$svc_count != 3) echo 'trigger-alright'; ?>">
                                        <?php echo get_sub_field('new_service_item_button_text'); ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>                
            </div>
        </div>
    </div>
</div>
