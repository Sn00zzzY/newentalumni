<section class="section features-section" id="features">
  <div class="container">
    <div class="block-features">

      <?php if( have_rows('feature_list') ): ?>

        <?php while ( have_rows('feature_list') ) : the_row(); ?>

          <h3 class="subtitle"><?php echo get_sub_field('feature_list_title'); ?></h3>
          <div class="five-column clearfix">

            <?php if( have_rows('features') ): ?>

              <?php while ( have_rows('features') ) : the_row(); ?>

                <div class="col">
                  <div class="description-holder clearfix">
                    <div class="icon">
                      <img src="<?php echo get_sub_field('feature_icon'); ?>" alt="feature-image">
                    </div>
                    <div class="description">
                      <h4><?php echo get_sub_field('feature_title'); ?></h4>
                      <p><?php echo get_sub_field('feature_content'); ?></p>
                    </div>
                  </div>
                </div>

              <?php endwhile; ?>

            <?php else : ?>
            <!--// no rows found-->
            <?php endif; ?>

          </div>

        <?php endwhile; ?>

      <?php else : ?>
      <!--no rows found-->
      <?php endif; ?>

    </div>
  </div>
</section>