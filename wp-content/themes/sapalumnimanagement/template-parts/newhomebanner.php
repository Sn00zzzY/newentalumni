<div class="banner" id="banner" style="background-image: url(<?php echo get_field('banner_background')['url']; ?>);">
    <div class="container">
        <h1 class="banner-title"><?php echo get_field('main_title'); ?></h1>
        <h2 class="banner-sub"><?php echo get_field('main_sub_title'); ?></h2>
        <h3 class="banner-subinfo"><?php echo get_field('sub_title'); ?></h3>
        <p class="banner-desc"><?php echo get_field('sub_description'); ?></p>
        <a class="btn btn-custom btn-blue with-border trigger-alright" href="<?php echo get_field('start_button_link'); ?>">Start Now</a>
    </div>
</div>