<section class="section promo-section notification-header-section">
    <div class="container text-center">
        <div class="title-holder">
            <div class="icon-holder">
                <img src="<?php echo get_field('notification_icon'); ?>" alt="Notificatoin Icon">
            </div>
            <h1><?php echo get_field('notification_title'); ?></h1>
            <div class="info">Last updated: <a href="#"><?php echo date('T H:i') ?></a></div>
            <p><?php echo get_field('notification_description'); ?></p>
        </div>
    </div>
</section>