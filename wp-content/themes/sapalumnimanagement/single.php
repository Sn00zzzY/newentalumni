<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage SAP_Alumni
 * @since SAP Alumni 1.0
 */
get_header(); ?>

<main class="main">
    <?php //get_template_part( 'template-parts/hero' ); ?>
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <div class="content-holder container">
            

            <div class="content">
                <div class="holder">
                    <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
                    <h1><?php echo get_the_title(); ?></h1>
                    <div class="blog-title">by <a
                                href="#"><?php echo get_the_author(); ?></a> <?php echo get_the_date('j M Y'); ?></div>
                    <div class="top-panel clearfix">
                        <?php
                            $tags_list = wp_get_post_tags($post->ID);
                            if ($tags_list) :
                        ?>
                            <ul class="tag-list">
                                <?php foreach ($tags_list as $single_tag) :?>
                                    <li><a href="<?php  echo get_tag_link($single_tag->term_id);?>"><?php echo $single_tag->name; ?></a></li>
                                <?php endforeach;?>
                            </ul>
                        <?php endif; ?>
                        <div class="social">
                            <span>Share</span>
                            <?php echo do_shortcode('[addtoany]')?>
                        </div>
                    </div>
                    <div class="text-holder">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>

            <?php get_sidebar('main'); ?>
        </div>
    <?php endwhile; ?>
    <?php endif; ?>
</main>

<?php get_footer(); ?>
