<?php
/**
 * Template Name: Competitor Template
 */
get_header(); ?>

<main class="main">
	<?php get_template_part( 'template-parts/page/competitor', 'hero'); ?>
	<?php get_template_part( 'template-parts/page/competitor', 'explore'); ?>
	<?php get_template_part( 'template-parts/page/alumni', 'contact'); ?>
	<?php get_template_part( 'template-parts/page/alumni-footer'); ?>
</main>

<?php get_footer(); ?>