<?php
/**
 * Template Name:  Template Thoughtleaders
 */
get_header(); ?>

    <main class="main">
        <section class="thoughtleader-hero parallax" style="<?php if (get_the_post_thumbnail_url()) :?> background-image: url(<?php echo get_the_post_thumbnail_url(); ?>) <?php endif; ?>; -webkit-background-size: cover;background-size: cover;">
            <div class="container container-normal">
                <h1><?=get_field('thoughtleade_hero_title')?></h1>
                <p><?=get_field('thoughtleade_hero_descr')?></p>
            </div>
        </section>
        <div class="content-container">
            <?php get_template_part('template-parts/page/hero-bottom'); ?>
            <?php get_template_part('template-parts/page/company-list'); ?>
        </div>
    </main>

<?php get_footer(); ?>