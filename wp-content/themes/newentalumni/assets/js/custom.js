
jQuery( document ).ready(function( $ ) {
	function header_sticly() {
		//Header Animation
		if($(window).innerWidth() > 767) {
			if($(this).scrollTop() > 100) {
				$('header').addClass('sticky-header');
			} else {
				$('header').removeClass('sticky-header');
			}
		} else {
			// $('header').addClass('sticky-header');
		}
	}

	$(document).on('click', '.navbar-toggle', function(){
		$('.navbar-toggle').closest('.navbar-header').toggleClass('open-menu');
	});


	
	function doParallax() {
		var top = $(this).scrollTop();
		var height = $(this).height();
		$parallaxs = $('.parallax');
		for(let i = 0; i < $parallaxs.length; i ++) {
			$parallax = $parallaxs.eq(i);
			var pTop = $parallax.offset().top;
			if((top - pTop) <= height) {
				$parallax.css('background-position-y', ((top - pTop) / 5) + 'px');
			}
		}
	}
	doParallax();
	
	$(window).scroll(function() {
		//Homepage Parallax
		doParallax();

		header_sticly();
	});

	header_sticly();

	$(window).resize(function(){
		header_sticly();
	});

	//bxslider
	if($('.comp-slide .bxslider').length) {
		$('.comp-slide .bxslider').bxSlider({
			mode: 'fade',
			auto: 'true',
			pause: 7000
		});
	}

	//Video Play
	$('.video-holder .btn-play').click(function() {
		var video = document.getElementById('intro-video');
		$('.video-holder .video-info').hide();
		video.play();
	})

	var $fadeSlider = $('.fade-slider');
	$fadeSlider.hide();

	//Contact Modal Trigger
	$('.contact-trigger').click(function(e) {
		e.preventDefault();
		$('#contactModal').modal('show');
	})

	//Placeholder Label Hide/Show
	$('.with-placeholder input').on('input', function() {
		if($(this).val()) {
			$(this).parent().parent().find('label').hide();
		} else {
			$(this).parent().parent().find('label').show();
		}
	})

	$(window).on('load', function() {
		$fadeSlider.show();
		//Homepage FAQ Slider
		$('.fade-slider .slide-item:gt(0)').hide();
		var whos = $('.fade-slider .slide-item');
		resizeSliderHeight();

		function resizeSliderHeight() {
			var height = 0;
			for(var i = 0; i < whos.length; i++) {
				whos.eq(i).show();
				if(whos.eq(i).children().innerHeight() > height) {
					height = whos.eq(i).children().innerHeight();
				}
				whos.eq(i).hide();
			}
			$fadeSlider.innerHeight(height + 50);	
		}

		function showNextSlide() {
			var $first = $('.fade-slider .slide-item:first');
			var $next = $first.next();
			var $firstTemp = $first.fadeOut(1000).detach();
			var $nextChild = $next.children();

			$next.fadeIn(1000);
			$fadeSlider.append($firstTemp);
		}
		
		showNextSlide();
		setInterval(function() { 
			showNextSlide();
		},  8000);

		//
		$(window).resize(function() {
			resizeSliderHeight();
		});
	});
});