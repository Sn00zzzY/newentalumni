/*

Suga Slider

Usage ---------

$(window).load(function(){
  $('#logos').suga({
    'transitionSpeed': 2000,
    'snap': false
  });
});

The markup should resemble the markup here

*/
(function($){
  $.fn.suga = function(options) {
    var settings = $.extend({
        'transitionSpeed': 3000,
        'snap': false,
        // 'slideMove': 2
    }, options);

    var $this = $(this);

    // add plugin specific classes
    $this.addClass('suga-slider-wrap');
    $this.children('ul').addClass('suga-slider-group');
    $this.find('li').addClass('suga-slide');  

    // caching $$$
    var $slide = $('.suga-slide'),
  			$firstEl = $('.suga-slide:first'),
        $group = $('.suga-slider-group'),
        $wrap = $('.suga-slider-wrap');

    var slideWidth = $slide.outerWidth(),
    		slideHeight = $('.suga-slide').height(),
        slideCount = $slide.length,
        totalWidth = slideWidth * slideCount;

    var snapReversed = false;

    // set width on group element
    $group.width(totalWidth);
    $wrap.height(slideHeight);
    $wrap.wrap('<div class="suga-container"></div>');

    // add first class at start
    if (!$group.find($firstEl).hasClass("suga-first")) {
      $group.find($firstEl).addClass("suga-first");
    }

    //calculate the width
    function calculateWidthSlider(){
      if (window.innerWidth > 1700) {
        var sliderCount = 9;
      } else if (window.innerWidth > 1200) {
        var sliderCount = 6;
      } else if (window.innerWidth > 991) {
        var sliderCount = 4;
      } else if (window.innerWidth > 767) {
        var sliderCount = 3;
      } else if (window.innerWidth > 450) {
        var sliderCount = 2;
      } else {
        var sliderCount = 1;
      }
      var wrapperSlider = $('#logos').outerWidth(),
          widthSlider = wrapperSlider/sliderCount;
      $('.suga-slide').css('width', widthSlider);
    }
    calculateWidthSlider();
    $(window).resize(function(){
      calculateWidthSlider();
    });



    // lets move baby
    var transitionSnap = function() {
      if(!snapReversed) {
        var $firstEl = $group.find('.suga-first').html();
        slideWidth = $group.find('.suga-first').outerWidth() //* settings.slideMove;
        
        $group.find('.suga-first').animate({
          'margin-left': '-' + slideWidth + 'px'
        }, function(){

         $group.append('<li class="suga-slide">' + $firstEl + '</li>');
         $(this).remove();

         $group.find('li:first').addClass("suga-first");
          
        });  
      } else {
        slideWidth = $group.find('.suga-slide:last-child').outerWidth() //* settings.slideMove;
        $group.find('.suga-first').removeClass('suga-first');
        var $lastEl = $group.find('li:last-child').detach().addClass('suga-first').css('margin-left', '-' + slideWidth + 'px');
        $group.prepend($lastEl);
        $group.find('.suga-first').animate({
          'margin-left': '0px'
        });
      }
    };

    var transitionScroll = function() {
       var $firstEl = $group.find('.suga-first').html();
      $group.find('.suga-first').animate({
        'margin-left': '-' + slideWidth + 'px'
      }, settings.transitionSpeed, 'linear', function(){
       $group.append('<li class="suga-slide">' + $firstEl + '</li>');
       $(this).remove(); 
       $group.find('li:first').addClass("suga-first");
       transitionScroll();
      });       
    };

    if (settings.snap) {
      window.setInterval(transitionSnap, settings.transitionSpeed);  
    } else {
      transitionScroll();
    }

    $('.btn-logo-next').click(function() {
      snapReversed = false;
      transitionSnap();
    });

    $('.btn-logo-prev').click(function() {
      snapReversed = true;
      transitionSnap();
    })
  }

  $(window).load(function(){

    if($('.slick-logos').length) {
      var prev = $('.slick-logos').closest('.logo-slider').find('.btn-logo-prev'),
          next = $('.slick-logos').closest('.logo-slider').find('.btn-logo-next');
      $('.slick-logos').slick({
        slidesToShow: 9,
        slidesToScroll: 9,
        autoplay: true,
        prevArrow: prev,
        nextArrow: next,
        infinite: true,
        speed: 1000,
        autoplaySpeed: 5000,
        responsive: [
          {
            breakpoint: 1700,
            settings: {
              slidesToShow: 6,
              slidesToScroll: 6,
            }
          },
          {
            breakpoint: 1200,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 4
            }
          },
          {
            breakpoint: 991,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3
            }
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 450,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      });
    } else {
      $('#logos').suga({
        'transitionSpeed': 5000,
        'snap': true
      });
    }
  });
})(jQuery);
