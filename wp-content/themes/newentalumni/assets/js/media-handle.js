jQuery( document ).ready(function( $ ) {
    //Form verification
    $('.section-download input.form-control').keydown(function() {
    	if($(this).val()) {
    		$(this).parent().removeClass('has-error');
    	}
    })

    $('.login-form .btn-submit').click(function() {
    	$('.login-form input.form-control').each(function() {
    		validateFields($(this));
    	})

    	if(!$('.login-form .has-error').length) {
    		$('.login-form').addClass('form-error');
    	}
    });

    $('.request-form .btn-submit').click(function() {
    	$('.request-form input.form-control').each(function() {
    		validateFields($(this));
    	})

    	if(!$('.request-form .has-error').length) {
    		$('.request-form').hide();
    		$('.content-title .subtitle').hide();
    		$('.thankyou-form').show();
    		thankyouFormRedirect(5);
    	}
    });

    $('.request-form .close').click(function() {
    	$('.login-form').show();
    	$('.request-block').show();
    	$('.request-form').hide();
    })

    $('.request-block button').click(function() {
    	$('.login-form').hide();
    	$('.request-block').hide();
    	$('.request-form').show();
    });

    $('.btn-goback').click(function() {
        window.location.replace('/');
    })

    function thankyouFormRedirect(second) {
    	$('.thankyou-form .notification span').html(second);

    	if(second == 0) {
    		window.location.replace('/');
    		return;
    	}

    	setTimeout(function() {
    		thankyouFormRedirect(second - 1);
    	}, 1000);
    }

    function showError($input) {
    	$input.parent().find('.error-info').remove();
    	$input.parent().append('<p class="error-info">Required Field</p>');
    	$input.parent().removeClass('.has-error').addClass('has-error');
    }

    function validateFields($field) {
    	if(!$field.val()) {
			showError($field);
		}

		if($field.attr('type') == 'email' && !validateEmail($field.val())) {
			showError($field);
		}
    }

    function validateEmail(email) {
	    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(email);
	}

});