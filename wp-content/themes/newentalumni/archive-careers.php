<?php
/**
 * Template Name: Jobs Template
 */

get_header(); ?>

<div class="job-page">
  <div class="blog-header">
      <div class="blog-back parallax" style="background-image: url(<?=get_template_directory_uri()?>/assets/images/careers-background-jpg.png)"></div>
      <div class="container container-normal">
          <h2 class="jobs-title">Join Us... Let The Innovation Begin!</h2>
          <h6 class="jobs-introtitle">Looking for your dream job? Join the EA team and begin your pathway to career success. Innovate, collaborate and deliver to the world’s largest companies!  </h6>
      </div>
  </div>
  <div class="blog-content">
    <div class="container-fluid">
        <h2 class="name-jobs">Current Openings:</h2>
        <div class="jobs-shell">
            <ul class="jobs-lists">
              <?php
                $paged = get_query_var('paged') ? get_query_var('paged') : 1;
                $careers = array('post_type' => 'careers', 'posts_per_page' => 9, 'paged' => $paged);
                $loop = new WP_Query($careers);
                while ($loop->have_posts()) :
                    $loop->the_post();
                    get_template_part( 'template-parts/careers/career', get_post_format() );
                endwhile; 
              ?>
            </ul>

            <?php
              the_posts_pagination( array(
                'prev_text' => '<' . '<span class="screen-reader-text">' . __( 'Previous page', 'twentyseventeen' ) . '</span>',
                'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'twentyseventeen' ) . '</span>' . '>',
                'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyseventeen' ) . ' </span>',
              ) );
            ?>

            
        </div>
    </div>
</div>

<?php get_footer();
