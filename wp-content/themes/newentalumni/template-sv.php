<?php
/**
 * Template Name: SV Template
 */
get_header(); ?>


<main class="main">
    <section class="sv-hero parallax" style="<?php if (get_the_post_thumbnail_url()) :?> background-image: url(<?php echo get_the_post_thumbnail_url(); ?>) <?php endif; ?>;  -webkit-background-size: cover;background-size: cover;">
        <div class="container container-normal">
            <div class="col-md-5">

                <?=get_field('sv_hero_content')?></p>

            </div>
            <div class="col-md-7">
                <image src="<?=get_field('sv_hero_image')?>"></image>
            </div>
        </div>
    </section>
  <?php get_template_part( 'template-parts/page/sv-info' ); ?>
  <?php get_template_part( 'template-parts/page/sv-contact' ); ?>
</main>

<?php get_footer(); ?>