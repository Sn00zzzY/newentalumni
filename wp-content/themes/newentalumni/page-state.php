<?php
/**
 * Template Name: State Template
 */
get_header(); ?>

<main class="main">
	<div class="state-page">
		<?php get_template_part( 'template-parts/page/state', 'hero'); ?>
		<?php get_template_part( 'template-parts/page/state', 'detail'); ?>
	</div>
</main>

<?php get_footer(); ?>