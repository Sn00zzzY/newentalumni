<li>
  <div class="jobs-outer">
      <div class="jobs-inner">
          <div class="jobs-head">
              <p class="jobs-date">Added  <?php echo get_the_date('m/d/Y'); ?></p>
              <p class="jobs-name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
          </div>
          <div class="jobs-middle">
              <?php if(!empty(get_field('location'))) { ?>
                <div class="jobs-location"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/map-gray.png" alt="location"/> <?=get_field('location');?></div>
              <?php } ?>
              <div class="jobs-btn">
                  <a href="<?php the_permalink(); ?>" class="btn btn-custom">More info <i class="fa fa-angle-right"></i></a>
              </div>
          </div>
      </div>
  </div>
</li>