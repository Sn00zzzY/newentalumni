<section class="infograph-section">
    <div class="container container-normal">
        <div class="infograph-inner">
            <div class="img-holder">
                <img src="<?=get_field('state_detail_infograph')?>" class="img-responsive">
            </div>
            <div class="image-desc">
                <p><?=get_field('state_detail_infograph_desc')?></p>
                <a href="<?=get_field('state_detail_file_link')?>" download><span>Download</span> this Infographic <i class="download-icon"></i></a>
            </div>
        </div>

        <div class="reach-block">
            <h2>Reach out!</h2>
            <p><?=get_field('state_detail_contact_us_text')?></p>
            <a href="" class="btn btn-blue btn-custom btn-angle" data-toggle="modal" data-target="#contactModal">Contact Us <i class="fa fa-angle-right"></i></a>
        </div>
    </div>
</section>