<section class="what-section">
    <div class="container content-container">
        <h4 class="section-title"><?=get_field('ea_home_what_title')?></h4>
        <p class="section-text"><?=get_field('ea_home_what_text')?></p>
    </div>
    <div class="row">
        <?php 
            if( have_rows('ea_home_what_list') ):
                while ( have_rows('ea_home_what_list') ) : the_row(); 
        ?>
            <div class="col-md-4 what-item">
                <div class="what-content" style="background-image: url(<?=get_sub_field('ea_home_what_item_background')?>) !important">
                    <h4><?=get_sub_field('ea_home_what_item_title');?></h4>
                    <p class="what-info"><?=get_sub_field('ea_home_what_item_text');?></p>
                    <p class="what-url">
                        <a href="<?=get_sub_field('ea_home_what_item_url');?>" class="btn btn-border <?=get_sub_field('ea_home_what_item_class_btn');?>">
                                <?php 
                                    if (!empty(get_sub_field('ea_home_what_item_anchor'))) echo get_sub_field('ea_home_what_item_anchor'); else echo "Learn More"; 
                                ?>
                                 <i class="fa fa-angle-right"></i>
                        </a>
                    </p>
                </div>
            </div>
        <?php 
                endwhile;
            endif; 
        ?>
    </div>
</section>