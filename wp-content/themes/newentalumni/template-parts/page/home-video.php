<section class="video-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-5 col-md-6 shell-image-video">
                <div class="video-image">
                    <img src="<?=get_field('ea_home_video_image')?>">
                </div>
                <div class="video-image video-image-xs">
                    <img src="<?=get_field('ea_home_video_xs_image')?>">
                </div>
            </div>

            <div class="col-sm-7 col-md-6 shell-data-video">
                <div class="video-content">
                    <h5><?=get_field('ea_home_video_title')?></h5>
                    <div class="video-holder">
                        <div class="video-info">
                            <div class="info-content">
                                <h6>An introduction to</h6>
                                <div class="logo-holder">
                                    <img src="<?=get_template_directory_uri()?>/assets/images/logo-color.png">
                                </div>
                                <div class="btn-play">
                                    <img src="<?=get_template_directory_uri()?>/assets/images/icon-play.png">
                                    <span>PLAY VIDEO</span>
                                </div>
                            </div>
                        </div>
                        <video class="video-shortcode" width="100%" loop="1" preload="metadata" controls="controls" id="intro-video"> 
                            <source type="video/mp4" src="<?=get_field('ea_home_video_src')?>">
                        </video>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>