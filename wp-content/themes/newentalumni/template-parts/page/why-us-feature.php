<section class="integration-info">
    <div class="container container-normal">
        <div class="info-blocks">
            <div class="info-left">
                <?php 
                    if( have_rows('integration_feature_list') ):
                        while ( have_rows('integration_feature_list') ) : the_row(); 
                ?>
                    <div class="info-item">
                        <h5><?=get_sub_field('integration_feature_title');?></h5>
                        <p><?=get_sub_field('integration_feature_text');?></p>
                    </div>
                <?php 
                        endwhile;
                    endif; 
                ?>
            </div>
            <div class="info-right">
                <?php 
                    if( have_rows('integration_feature_logo_list') ):
                        while ( have_rows('integration_feature_logo_list') ) : the_row(); 
                ?>
                    <div class="info-logo">
                        <img src="<?=get_sub_field('integration_feature_logo');?>">
                        <h5><?=get_sub_field('integration_feature_title');?></h5>
                        <p><?=get_sub_field('integration_feature_text');?></p>
                    </div>
                <?php 
                        endwhile;
                    endif; 
                ?>
            </div>
        </div>
    </div>
</section>
