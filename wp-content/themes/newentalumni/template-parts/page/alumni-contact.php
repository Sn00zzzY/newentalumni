<section class="contact-section parallax">
    <div class="container">
        <div class="container-inner inner-lg">
            <h4><span><?=get_field('ea_contact_title', option)?></span></h4>
            <div class="contact-form">
                <?php echo do_shortcode(get_field('ea_contact_form_code', option)); ?>
            </div>
        </div>
    </div>
</section>