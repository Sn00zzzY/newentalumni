<section class="section promo-section" id="home">
    <div class="container">

        <div class="img-holder">
            <img src="<?php echo get_field('hero_image'); ?>" alt="hero image" class="img-responsive">
        </div>
        <div class="title-holder">
            <?php echo get_field('hero_content'); ?>
        </div>

</section>