<section class="other-section">
    <div class="container">
        <h6><?=get_field('ea_home_other_title')?></h6>
        <div class="other-items">
            <div class="other-item other-market">
                <p><?=get_field('ea_other_market_text')?></p>
                <div class="other-info">
                    <img src="<?=get_template_directory_uri()?>/assets/images/logo2.png">
                    <a href="<?=get_field('ea_other_market_link')?>" class="btn-market" target="_blank">Visit Alumni Marketplace <i class="fa fa-angle-right"></i></a>
                </div>
                <div class="ipad-image">
                    <img src="<?=get_field('ea_other_market_ipad')?>">
                </div>
            </div>
            <div class="other-item other-gigs">
                <div class="other-top">
                    <img src="<?=get_template_directory_uri()?>/assets/images/logo-gigs.png">
                    <p><?=get_field('ea_other_gigs_text')?></p>
                </div>
                <a href="<?=get_field('ea_other_gigs_link')?>" class="btn-gigs" target="_blank">VISIT ALUMNI GIGS <i class="fa fa-angle-right"></i></a>
                <div class="gigs-bottom"></div>
            </div>
        </div>
    </div>
</section>