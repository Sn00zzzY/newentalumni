<section class="state-hero parallax">
    <div class="container container-normal">
        <h1><?=get_field('state_hero_title')?></h1>
        <p><?=get_field('state_hero_text')?></p>
    </div>
</section>
