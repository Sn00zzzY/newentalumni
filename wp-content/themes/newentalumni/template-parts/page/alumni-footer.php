<div class="alumni-section">
    <div class="container">
        <div class="alumni-ref">
            <p>EnterpriseAlumni is proud to be:</p>
            <?php 
                if( have_rows('ea_partner_list', option) ):
                    while ( have_rows('ea_partner_list', option) ) : the_row(); 
            ?>
            	<a href="<?=get_sub_field('ea_partner_item_link')?>"><img src="<?=get_sub_field('ea_partner_item_image')?>" alt="<?=get_sub_field('ea_partner_item_image_alt')?>"></a>
            <?php 
                    endwhile;
                endif; 
            ?>
        </div>
    </div>
</div>
