<section class="comp-section parallax">
    <div class="container">
        <div class="comp-inner">
            <div class="comp-content">
                <h1><?=get_field('competitor_hero_title');?></h1>
                <p><?=get_field('competitor_hero_text');?></p>
            </div>
            <div class="comp-slide">
                <ul class="bxslider">
                    <?php 
                        if( have_rows('competitor_hero_slider') ):
                            while ( have_rows('competitor_hero_slider') ) : the_row(); 
                    ?>
                        <li><img src="<?=get_sub_field('competitor_hero_slide');?>"></li>
                    <?php 
                            endwhile;
                        endif; 
                    ?>
                </ul>
            </div>
        </div>
    </div>
</section>
