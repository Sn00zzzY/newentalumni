<section class="which-section">
    <div class="container">
        <div class="which-inner">
            <h4 class="section-title"><?=get_field('ea_home_which_title')?></h4>
            <p><?=get_field('ea_home_which_text')?></p>
            <div class="service-info">
                <h6><?=get_field('ea_home_which_service_title')?></h6>
                <div class="svg-wrap">
                    <svg height="370px" class="svg-block hidden-xs">
                        <?php if( have_rows('right_side') ):
                            $i = 0;
                            while ( have_rows('right_side') ) : the_row();

                                $imageLogo = get_sub_field('image'); ?>
                                <line x1="50%" y1="50%" id="right-line-<?php echo $i?>"
                                      x2="50%"
                                      y2="50%"
                                      style="stroke: #D2DCE0;stroke-width:2" />

                                <animate
                                        xlink:href="#right-line-<?php echo $i?>"
                                        attributeName="x2"
                                        from="50%"
                                        to="<?php the_sub_field('x-coord'); ?>%"
                                        dur="2s"
                                        begin="<?php echo $i/2?>s"
                                        repeatCount="1"
                                        fill="freeze"
                                        id="line-anim-<?php echo $i?>"/>

                                <animate
                                        xlink:href="#right-line-<?php echo $i?>"
                                        attributeName="y2"
                                        from="50%"
                                        to="<?php the_sub_field('y-coord'); ?>%"
                                        dur="2s"
                                        begin="<?php echo $i/2?>s"
                                        repeatCount="1"
                                        fill="freeze"
                                        id="line-anim-vert-<?php echo $i?>"/>

                                <circle stroke="#D2DCE0" id="right-cicle-<?php echo $i?>"
                                        stroke-width="7"
                                        r="<?php the_sub_field('circle_radius'); ?>"
                                        cx="50%"
                                        cy="50%"
                                        fill="#fff" />

                                <animate
                                        xlink:href="#right-cicle-<?php echo $i?>"
                                        attributeName="cx"
                                        from="50%"
                                        to="<?php the_sub_field('x-coord'); ?>%"
                                        dur="2s"
                                        begin="<?php echo $i/2?>s"
                                        repeatCount="1"
                                        fill="freeze"
                                        id="circ-anim-<?php echo $i?>"/>

                                <animate
                                        xlink:href="#right-cicle-<?php echo $i?>"
                                        attributeName="cy"
                                        from="50%"
                                        to="<?php the_sub_field('y-coord'); ?>%"
                                        dur="2s"
                                        begin="<?php echo $i/2?>s"
                                        repeatCount="1"
                                        fill="freeze"
                                        id="circ-anim-vert-<?php echo $i?>"/>

                                <image id="right-image-<?php echo $i?>"
                                       width="<?php echo $imageLogo['width'];?>"
                                       height="<?php echo $imageLogo['height'];?>"
                                       x="50%"
                                       y="50%"
                                       transform="translate(-<?php echo $imageLogo['width'] /2 ;?>, -<?php echo $imageLogo['height'] /2 ;?>)"
                                       xlink:href="<?php echo $imageLogo['url']; ?>"/>

                                <animate
                                        xlink:href="#right-image-<?php echo $i?>"
                                        attributeName="x"
                                        from="50%"
                                        to="<?php the_sub_field('x-coord'); ?>%"
                                        dur="2s"
                                        begin="<?php echo $i/2?>s"
                                        repeatCount="1"
                                        fill="freeze"
                                        id="image-anim-<?php echo $i?>"/>

                                <animate
                                        xlink:href="#right-image-<?php echo $i?>"
                                        attributeName="y"
                                        from="50%"
                                        to="<?php the_sub_field('y-coord'); ?>%"
                                        dur="2s"
                                        begin="<?php echo $i/2?>s"
                                        repeatCount="1"
                                        fill="freeze"
                                        id="image-anim-vert-<?php echo $i?>"/>

                                <?php
                                $i++;
                            endwhile;
                        endif; ?>

                        <?php if( have_rows('left_side') ):
                            $i = 0;
                            while ( have_rows('left_side') ) : the_row();

                                $word = get_sub_field('name');
                                $word = strtoupper($word)?>
                                <line x1="50%" y1="50%" id="left-line-<?php echo $i?>"
                                      x2="50%" y2="50%"
                                      style="stroke: #1EA9E4;stroke-width:2" />

                                <animate
                                        xlink:href="#left-line-<?php echo $i?>"
                                        attributeName="x2"
                                        from="50%"
                                        to="<?php the_sub_field('x-coord'); ?>%"
                                        dur="1s"
                                        begin="<?php echo $i/3?>s"
                                        repeatCount="1"
                                        fill="freeze"
                                        id="line2-anim-<?php echo $i?>"/>

                                <animate
                                        xlink:href="#left-line-<?php echo $i?>"
                                        attributeName="y2"
                                        from="50%"
                                        to="<?php the_sub_field('y-coord'); ?>%"
                                        dur="1s"
                                        begin="<?php echo $i/3?>s"
                                        repeatCount="1"
                                        fill="freeze"
                                        id="line2-anim-vert-<?php echo $i?>"/>

                                <circle stroke="#1EA9E4" id="left-circle-<?php echo $i?>"
                                        stroke-width="7"
                                        r="<?php the_sub_field('circle_radius'); ?>"
                                        cx="50%" cy="50%"
                                        fill="#fff">
                                </circle>

                                <animate
                                        xlink:href="#left-circle-<?php echo $i?>"
                                        attributeName="cx"
                                        from="50%"
                                        to="<?php the_sub_field('x-coord'); ?>%"
                                        dur="1s"
                                        begin="<?php echo $i/3?>s"
                                        repeatCount="1"
                                        fill="freeze"
                                        id="circle2-anim-<?php echo $i?>"/>

                                <animate
                                        xlink:href="#left-circle-<?php echo $i?>"
                                        attributeName="cy"
                                        from="50%"
                                        to="<?php the_sub_field('y-coord'); ?>%"
                                        dur="1s"
                                        begin="<?php echo $i/3?>s"
                                        repeatCount="1"
                                        fill="freeze"
                                        id="circle2-anim-vert-<?php echo $i?>"/>

                                <text id="left-text-<?php echo $i?>"
                                      x="50%" y="50%"
                                      text-anchor="middle" dy=".3em" fill="#000"
                                      style="font-family: Roboto; font-weight: bold;">
                                    <?php echo $word ?>
                                </text>

                                <animate
                                        xlink:href="#left-text-<?php echo $i?>"
                                        attributeName="x"
                                        from="50%"
                                        to="<?php the_sub_field('x-coord'); ?>%"
                                        dur="1s"
                                        begin="<?php echo $i/3?>s"
                                        repeatCount="1"
                                        fill="freeze"
                                        id="text2-anim-<?php echo $i?>"/>

                                <animate
                                        xlink:href="#left-text-<?php echo $i?>"
                                        attributeName="y"
                                        from="50%"
                                        to="<?php the_sub_field('y-coord'); ?>%"
                                        dur="1s"
                                        begin="<?php echo $i/3?>s"
                                        repeatCount="1"
                                        fill="freeze"
                                        id="text2-anim-vert-<?php echo $i?>"/>


                                <?php $i++;
                            endwhile;

                        endif;

                        ?>

                        <circle stroke="#A2AEB2"
                                stroke-width="8"
                                r="112" cx="50%" cy="50%"
                                fill="#fff">
                        </circle>

                        <?php
                        $image = get_field('main_circle'); ?>

                        <image width="<?php echo $image['width'];?>"
                               height="<?php echo $image['height'];?>"
                               x="50%" y="50%"
                               transform="translate(-<?php echo $image['width'] /2 ;?>, -<?php echo $image['height'] /2 ;?>)"
                               xlink:href="<?php echo $image['url']; ?>"/>

                    </svg>
                    <svg height="560px" class="svg-block-mobile visible-xs">

                        <?php if( have_rows('right_side') ):
                            $i = 0;
                            while ( have_rows('right_side') ) : the_row();

                                $imageLogo = get_sub_field('image'); ?>
                                <line x1="50%" y1="59%" id="right-line-<?php echo $i?>"
                                      x2="<?php the_sub_field('mobile-x-coord'); ?>%"
                                      y2="<?php the_sub_field('mobile-y-coord'); ?>%"
                                      style="stroke: #D2DCE0;stroke-width:1" />

                                <circle stroke="#D2DCE0" id="right-cicle-<?php echo $i?>"
                                        stroke-width="3"
                                        r="<?php echo get_sub_field('mobile-radius')/2; ?>"
                                        cx="<?php the_sub_field('mobile-x-coord'); ?>%"
                                        cy="<?php the_sub_field('mobile-y-coord'); ?>%"
                                        fill="#fff" />

                                <image id="right-image-<?php echo $i?>"
                                       width="<?php echo $imageLogo['width']/1.5;?>"
                                       height="<?php echo $imageLogo['height']/1.5;?>"
                                       x="<?php the_sub_field('mobile-x-coord'); ?>%"
                                       y="<?php the_sub_field('mobile-y-coord'); ?>%"
                                       transform="translate(-<?php echo $imageLogo['width'] /3 ;?>, -<?php echo $imageLogo['height'] /3 ;?>)"
                                       xlink:href="<?php echo $imageLogo['url']; ?>"/>

                                <?php
                                $i++;
                            endwhile;

                        endif;

                        ?>

                        <?php if( have_rows('left_side') ):
                            $i = 0;
                            while ( have_rows('left_side') ) : the_row();

                                $word = get_sub_field('name');
                                $word = strtoupper($word)?>
                                <line x1="50%" y1="59%" id="left-line-<?php echo $i?>"
                                      x2="<?php the_sub_field('mobile-x-coord'); ?>%"
                                      y2="<?php the_sub_field('mobile-y-coord'); ?>%"
                                      style="stroke: #1EA9E4;stroke-width:1" />

                                <circle stroke="#1EA9E4" id="left-circle-<?php echo $i?>"
                                        stroke-width="2"
                                        r="<?php echo get_sub_field('mobile-radius')/2; ?>"
                                        cx="<?php the_sub_field('mobile-x-coord'); ?>%"
                                        cy="<?php the_sub_field('mobile-y-coord'); ?>%"
                                        fill="#fff">
                                </circle>

                                <text id="left-text-<?php echo $i?>"
                                      x="<?php the_sub_field('mobile-x-coord'); ?>%"
                                      y="<?php the_sub_field('mobile-y-coord'); ?>%"
                                      text-anchor="middle" dy=".3em" fill="#000"
                                      style="font-family: Roboto; font-weight: bold; font-size: 10px;">
                                    <?php echo $word ?>
                                </text>

                                <?php $i++;
                            endwhile;

                        endif;

                        ?>

                        <circle stroke="#A2AEB2"
                                stroke-width="4"
                                r="57" cx="50%" cy="59%"
                                fill="#fff">
                        </circle>

                        <?php
                        $image = get_field('main_circle'); ?>

                        <image width="<?php echo $image['width']/2;?>"
                               height="<?php echo $image['height']/2;?>"
                               x="50%" y="59%"
                               transform="translate(-<?php echo $image['width'] /4 ;?>, -<?php echo $image['height'] /4 ;?>)"
                               xlink:href="<?php echo $image['url']; ?>"/>

                    </svg>
                </div>
            </div>

            <div class="logo-slider">
                <div class="btn-logo-prev">
                    <img src="<?=get_template_directory_uri()?>/assets/images/icon-angle-left.png" alt="Logo Blue">
                </div>
                <div class="slider-holder">
                    <div id="logos">
                        <ul class="clearfix slick-logos">
                            <?php 
                                    if( have_rows('ea_home_which_brands') ):
                                        while ( have_rows('ea_home_which_brands') ) : the_row(); 
                                ?>
                                <li>
                                    <div class="shell-img-slider">
                                        <div class="img-which">
                                            <img src="<?=get_sub_field('ea_home_which_brand_image')?>" alt="<?=get_sub_field('ea_home_which_brand_alt')?>"/>
                                        </div>
                                        <div class="img-which-hover">
                                            <img src="<?=get_sub_field('ea_home_which_brand_image_color')?>" alt="<?=get_sub_field('ea_home_which_brand_alt_color')?>"/>
                                        </div>
                                    </div>
                                </li>
                            <?php 
                                    endwhile;
                                endif; 
                            ?>
                        </ul>
                    </div>
                </div>
                <div class="btn-logo-next">
                    <img src="<?=get_template_directory_uri()?>/assets/images/icon-angle-right.png" alt="Logo Blue">
                </div>
            </div>
            <div class="btn-holder">
                <a href="<?=get_field('ea_home_which_link')?>" class="btn btn-custom btn-white">Learn more <i class="fa fa-angle-right"></i></a>
            </div>
        </div>
    </div>
</section>