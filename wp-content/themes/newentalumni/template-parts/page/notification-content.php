<section class="notification-info">
  <div class="container container-normal">
    <div class="row">
      <div class="col-sm-5 col-md-4 notification-left">
        <div class="service-nitems">
          <div class="sub-title">Service Notifications</div>
          <?php if (have_rows('services_repeater')): ?>
            <div class="service-holder">
            <?php while (have_rows('services_repeater')) : the_row(); ?>
            <div class="service-nitem">
              <?php $status = get_sub_field('service_status'); ?>
              <div>
                <h6><?php the_sub_field('service_name'); ?></h6>
                <p>
                  <?php if ($status === 'working'): ?>
                    Service is operating normally
                  <?php else: ?>
                    <?php the_sub_field('not_working_reason'); ?>
                  <?php endif; ?>
                </p>
              </div>
              <?php if ($status === 'working'): ?>
                <span></span>
              <?php endif; ?>
            </div>
            <?php endwhile; ?>
          </div>
          <?php endif; ?>
        </div>
        <div class="scheduled-dbox">
          <h6>Scheduled Downtime</h6>
          <p>
            <?php if (get_field('sheduled_downtime') != NUll or get_field('sheduled_downtime') != '' or get_field('sheduled_downtime')) : ?>
              <?php the_field('sheduled_downtime'); ?>
            <?php else: ?>
              There is currently no upcoming scheduled downtime
            <?php endif; ?>
          </p>
        </div>
      </div>
      <div class="col-sm-7 col-md-8 notification-right">
        <div class="notification-holder">
          <h2>Notifications</h2>
          <div class="info">If you are experiencing an issue not reflected here please <span style="white-space:nowrap"><a href="#">contact support</a>.</span></div>
          <?php
            $args = array('post_type' => 'tickets', 'posts_per_page' => 7);
            $loop = new WP_Query($args);
            while ($loop->have_posts()) :
                $loop->the_post(); ?>
                <div class="notification-block">
                    <div class="title-notification"><?php echo get_the_date('F d Y, T H:i'); ?>
                        <span class="ticket"><?php the_title(); ?></span>
                    </div>
                    <p><?php echo strip_tags(get_the_content());?></p>
                </div>
            <?php endwhile; 
          ?>
      </div>
    </div>
  </div>
</section>