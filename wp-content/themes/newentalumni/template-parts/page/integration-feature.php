<section class="info-blocks-outer">
    <div class="container-fluid container-normal">
        <div class="info-inner">
            <?php 
                if( have_rows('integration_feature_list') ):
                    while ( have_rows('integration_feature_list') ) : the_row(); 
            ?>
                <div class="info-item">
                    <h5><?=get_sub_field('integration_feature_title');?></h5>
                    <p><?=get_sub_field('integration_feature_text');?></p>
                </div>
            <?php 
                    endwhile;
                endif; 
            ?>
        </div>
        <div class="info-inner-logos">
            <?php 
                if( have_rows('integration_feature_logo_list') ):
                    while ( have_rows('integration_feature_logo_list') ) : the_row(); 
            ?>
                <div class="info-logo">
                    <img src="<?=get_sub_field('integration_feature_logo');?>">
                </div>
            <?php 
                    endwhile;
                endif; 
            ?>
        </div>
    </div>
</section>

<section class="why-us-lists">
    <div class="why-us-lists-outer">

        <?php 
            if( have_rows('featured') ):
                while ( have_rows('featured') ) : the_row(); 
        ?>
                    <div class="col-sm-6 item-why">
                        <div class="inner-lists-feutured" style="background-image: url(<?=get_sub_field('featured_background');?>)">
                            <p class="name-featured-lists"><?=get_sub_field('featured_name');?></p>
                            <div class="text-featured-lists">
                                <?=get_sub_field('featured_text');?>
                            </div>
                        </div>
                    </div>
                <?php  endwhile;
        endif; ?>
    </div>
</section>

<section class="integration-logos-bottom">
    <div id="logos-bottom">
        <ul>
            <?php 
                    if( have_rows('featured_logos_bottom') ):
                        while ( have_rows('featured_logos_bottom') ) : the_row(); 
                ?>
                <li>
                    <div class="shell-logos-bottom">
                        <div class="logos-img-bottom">
                            <img src="<?=get_sub_field('image_logo')?>" alt="<?=get_sub_field('image_logo_alt')?>"/>
                        </div>
                        <div class="logos-img-bottom-hover">
                            <img src="<?=get_sub_field('image_logo_color')?>" alt="<?=get_sub_field('image_logo_alt')?>"/>
                        </div>
                    </div>
                </li>
            <?php 
                    endwhile;
                endif; 
            ?>
        </ul>
    </div>
    <div class="logos-bottom-note">And Many More</div>
</section>
