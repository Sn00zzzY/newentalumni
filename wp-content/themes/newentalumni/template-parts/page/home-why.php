<section class="why-section parallax">
    <div class="container">
        <div class="why-inner">
            <h4 class="section-title"><?=get_field('ea_home_why_title')?></h4>
            <?php 
                if( have_rows('ea_home_why_list') ):
                    while ( have_rows('ea_home_why_list') ) : the_row(); 
            ?>
                <div class="why-info">
                    <h3><?=get_sub_field('ea_home_why_list_title');?></h3>
                    <div class="why-detail">
                        <?php 
                            if( have_rows('ea_home_why_sublist') ):
                                while ( have_rows('ea_home_why_sublist') ) : the_row(); 
                        ?>
                            <div class="why-item">
                                <span><?=get_sub_field('ea_home_why_sub_item_title');?></span>
                                <p><?=get_sub_field('ea_home_why_sub_item_text');?></p>
                            </div>
                        <?php 
                                endwhile;
                            endif; 
                        ?>
                    </div>
                </div>
            <?php 
                    endwhile;
                endif; 
            ?>
        </div>
    </div>
</section>
