<section class="who-section">
    <div class="container">
        <div class="who-inner">
            <h4 class="section-title">
                <?=get_field('ea_home_who_title')?>
            </h4>
            <div class="fade-slider">
                <?php 
                        if( have_rows('ea_home_who_list') ):
                            while ( have_rows('ea_home_who_list') ) : the_row(); 
                    ?>
                        <div class="slide-item">
                            <div class="luft-block">
                                <div class="luft-comment">
                                    <div class="logo-holder">
                                        <img src="<?=get_sub_field('ea_home_who_item_logo');?>">
                                    </div>
                                    <p><?=get_sub_field('ea_home_who_item_text');?></p>
                                </div>
                                <div class="luft-image">
                                    <img src="<?=get_sub_field('ea_home_who_item_image');?>">
                                </div>
                            </div>
                        </div>
                    <?php 
                            endwhile;
                        endif; 
                    ?>
            </div>
        </div>
    </div>
</section>