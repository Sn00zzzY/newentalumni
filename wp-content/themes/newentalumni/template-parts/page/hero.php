<section class="thoughtleaders-hero hero parallax" style="<?php if (get_the_post_thumbnail_url()) :?> background-image: url(<?php echo get_the_post_thumbnail_url(); ?>) <?php endif; ?>">
    <div class="container container-normal">
        <h1><?=get_field('hero_content')?></h1>
        <p><?=get_field('integration_hero_text')?></p>
    </div>
</section>
