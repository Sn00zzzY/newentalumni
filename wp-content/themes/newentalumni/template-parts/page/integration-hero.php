<section class="integration-hero parallax" style="<?php if (get_the_post_thumbnail_url()) :?> background-image: url(<?php echo get_the_post_thumbnail_url(); ?>) <?php endif; ?>">
    <div class="container container-normal">
        <h1><?=get_field('integration_hero_title')?></h1>
        <p><?=get_field('integration_hero_text')?></p>
    </div>
</section>

<section class="cog-section">
    <div class="container-fluid container-normal">
        <div class="cog-outer">
            <div class="row row-flex flex-align-center">
                <div class="col-md-7">
                    <div class="cog-holder">
                        <img src="<?=get_field('integration_cog_image')?>">
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="info-cog-outer">
                        <p class="name-cog"><?=get_field('integration_cog_name')?></p>
                        <div class="data-cog">
                            <?=get_field('integration_cog_text')?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

