<section class="hero-section parallax">
    <div class="container">
        <div class="hero-inner">
            <div class="hero-image">
                <img src="<?=get_field('ea_home_hero_image')?>">
            </div>
            <div class="hero-text">
                <h2><?=get_field('ea_home_hero_title')?></h2>
                <p><?=get_field('ea_home_hero_text')?></p>
            </div>
        </div>
    </div>
</section>