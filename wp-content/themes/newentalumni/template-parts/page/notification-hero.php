<section class="notification-hero parallax">
  <div class="container container-normal">
    <div class="icon-holder">
      <img src="<?=get_field('notification_item');?>">
    </div>
    <h1><?=get_field('notification_title');?></h1>
    <h6><?=get_field('notification_time');?></h6>
    <p><?=get_field('notification_text');?></p>
  </div>
</section>
