<section class="explore-section">
    <div class="container">
        <div class="section-content">
            <div class="section-left">
                <h3><?=get_field('comp_explore_title');?></h3>
                <p><?=get_field('comp_explore_text');?></p>
            </div>
            <div class="section-right">
                <div class="img-holder">
                    <img src="<?=get_field('comp_explore_image');?>" class="img-responsive">
                </div>
                <p class="download-pdf"><a href="<?=get_field('comp_explore_file_link');?>" download><i class="download-icon"></i> <span>Download</span> this Infographic</a></p>
            </div>
        </div>
    </div>
</section>
