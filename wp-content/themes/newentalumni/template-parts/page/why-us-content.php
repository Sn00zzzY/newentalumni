<section class="why-us-lists">
    <div class="why-us-lists-outer">

        <?php 
            if( have_rows('featured') ):
                while ( have_rows('featured') ) : the_row(); 
        ?>
                    <div class="col-sm-6 item-why">
                        <div class="inner-lists-feutured" style="background-image: url(<?=get_sub_field('featured_background');?>)">
                            <p class="name-featured-lists"><?=get_sub_field('featured_name');?></p>
                            <div class="text-featured-lists">
                                <?=get_sub_field('featured_text');?>
                            </div>
                        </div>
                    </div>
                <?php  endwhile;
        endif; ?>
    </div>
</section>
 <section class="logos-fuetured">
    <div class="container-fluid container-normal">
        <div class="logos-fuetured-outer">
            <ul class="logos-fuetured-lists">
                <?php 
                    if( have_rows('featured_logos') ):
                        while ( have_rows('featured_logos') ) : the_row(); 
                ?>
                            <li>
                                <img src="<?=get_sub_field('featured_logos_image');?>">
                            </li>
                <?php 
                        endwhile;
                    endif; ?>
            </ul>
        </div>
    </div>
 </section>