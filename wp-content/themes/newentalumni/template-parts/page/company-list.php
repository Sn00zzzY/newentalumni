<section class="section company-info-section">
  <div class="container">
  <?php if( have_rows('company_list') ): ?>
    <?php while ( have_rows('company_list') ) : the_row(); ?>

      <article class="block-company">
        <div class="img-holder">
          <img src="<?php echo get_sub_field('company_image'); ?>" alt="company img">
        </div>
        <div class="description">
          <h2>
            <?php echo get_sub_field('company_title'); ?>
          </h2>

            <?php echo get_sub_field('company_description'); ?>

          <a href="<?php echo get_sub_field('company_link'); ?>" class="btn btn-primary" target="_blank">Visit Site  <i class="fa fa-angle-right"></i></a>
        </div>
      </article>

    <?php endwhile; ?>

  <?php else : ?>
<!--// no rows found-->
  <?php endif; ?>
  </div>
</section>
