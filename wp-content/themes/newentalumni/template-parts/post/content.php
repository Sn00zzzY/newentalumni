<div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
  <?php
    $post_url = get_template_directory_uri() . "/assets/images/EA-FeaturedBlogImage_preview.jpeg";
    if(get_the_post_thumbnail_url()) {
      $post_url = get_the_post_thumbnail_url();
    }
  ?>

  <div class="blog-image" style="background-image: url(<?php echo $post_url; ?>)">
  </div>
  <div class="item-content">
  	<a class="item-title" href="<?php the_permalink();?>"><?php the_title(); ?></a>
    <span class="blog-info">by <span class="post-author"><?php echo get_the_author_link(); ?></span> in <?php the_category(); ?></span>
    <span class="blog-info"><?php echo get_the_date('jS F Y'); ?></span>
    <p><?php echo wp_trim_words( get_the_content(), 35, ' [...]' ); ?></p>
  </div>
</div>
