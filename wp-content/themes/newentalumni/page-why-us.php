<?php
/**
 * Template Name: Why Us Template
 */
get_header(); ?>

<main class="main">
	<?php get_template_part( 'template-parts/page/why-us', 'hero'); ?>
	<?php get_template_part( 'template-parts/page/why-us', 'content'); ?>
</main>

<?php get_footer(); ?>