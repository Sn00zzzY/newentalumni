<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="blog-page">
  <div class="blog-header blog-list-header parallax">
    <div class="container container-normal">
      <?php if ( have_posts() ) : ?>
				<h2 class="page-title"><?php printf( __( 'Search Results for: %s', 'twentyseventeen' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
			<?php else : ?>
				<h2 class="page-title"><?php _e( 'Nothing Found', 'twentyseventeen' ); ?></h2>
			<?php endif; ?>
    </div>
  </div>
  <div class="blog-content">
    <div class="container container-normal">
      <div class="blog-inner blog-list">
        <div class="row">
          <div class="col-sm-8 blog-left">
            <?php if ( have_posts() ) : ?>
              <div class="blog-items">

                <?php
                  // Start the Loop.
                  while ( have_posts() ) : the_post();
                    get_template_part( 'template-parts/post/content' );

                  // End the loop.
                  endwhile;
                ?>
              </div>
              <?php
              the_posts_pagination( array(
									'prev_text' => twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous page', 'twentyseventeen' ) . '</span>',
									'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'twentyseventeen' ) . '</span>' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ),
									'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyseventeen' ) . ' </span>',
								) );
           	?>
            <?php endif; ?>

          </div>
          <div class="col-sm-4 blog-right">
            <?php get_sidebar(); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php get_footer();
