<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>


	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<section class="error-404 not-found">
				<div class="blog-header blog-list-header parallax" style="background-position-y: -6.4px;">
				    <div class="container container-normal">
				    	<h1>404</h1>
						<h3>Page not found</h3>
						<p>Unfortunately we can’t find the page you’re looking for. Sorry.</p>
				    </div>
				</div>

				<div class="container-fluid">
					<div class="wrapper-url-offer">
						<p class="name-url-offer">Maybe try one of these instead:</p>
						<div class="wrapper-lists-url-offer">
							<ul class="lists-url-offer">
								<li>
									<div class="screen">
										<img src="<?=get_template_directory_uri()?>/assets/images/screenshots/screenshot1.png">
									</div>
									<p class="url-offer"><a href="/state/">Read The State of Alumni 2017 Infographic</a></p>
								</li>

								<li>
									<div class="screen">
										<img src="<?=get_template_directory_uri()?>/assets/images/screenshots/screenshot2.png">
									</div>
									<p class="url-offer"><a href="/blog/">View the latest blog pieces</a></p>
								</li>

								<li>
									<div class="screen">
										<img src="<?=get_template_directory_uri()?>/assets/images/screenshots/screenshot3.png">
									</div>
									<p class="url-offer"><a href="/">Head back home</a></p>
								</li>
							</ul>
						</div>
					</div>
				</div>



				
			</section><!-- .error-404 -->
		</main><!-- #main -->
	</div><!-- #primary -->


<?php get_footer();
