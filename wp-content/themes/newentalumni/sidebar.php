<?php
/**
* The sidebar containing the main widget area
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package WordPress
* @subpackage Twenty_Seventeen
* @since 1.0
* @version 1.0
*/

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
return;
}
?>

<div class="widget">
<h6 class="widget-title">Find a blog post</h6>
<form role="search" method="get" id="searchform" class="searchform search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
  <input class="form-control" placeholder="Search" type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" />
  <i>
    <img src="<?=get_template_directory_uri()?>/assets/images/icon-search.png">
  </i>
</form>
<span class="widget-subtitle">Search our Archives:</span>
<div class="archive-widget dropdown widget-wrapper"> 
  <select class="selectpicker" onchange="document.location.href=this.options[this.selectedIndex].value;">
    <option value="">-- Please Select --</option>
    <?php wp_get_archives( array( 'type' => 'monthly', 'format' => 'option', 'show_post_count' => 1 ) ); ?>
  </select>
</div>
</div>
<div class="widget">
<?php
  $args = array(
    'numberposts' => 5,
    'offset' => 0,
    'category' => 0,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'include' => '',
    'exclude' => '',
    'meta_key' => '',
    'meta_value' =>'',
    'post_type' => 'post',
    'post_status' => 'publish',
    'suppress_filters' => true
  );

  $recent_posts = wp_get_recent_posts( $args, ARRAY_A ); 
?>

<h6 class="widget-title">Popular posts</h6>
<?php foreach ( $recent_posts as $rpost ) { ?>
  <?php if($rpost[ID] != $post->ID): ?>
    <div class="post-item">
      <a href="<?php echo get_permalink($rpost[ID]); ?>"><?php echo $rpost[post_title]; ?> <i class="fa fa-angle-right"></i></a>
      <span><?php echo get_the_date('jS F Y', $rpost); ?></span>
    </div>
  <?php endif; ?>
  <?php $i++; ?>
<?php } ?>
</div>
<div class="widget">
<h6 class="widget-title">Categories</h6>
  <?php $update_category = 3;?>
  <?php $categories = get_categories( array(
    'orderby' => 'name',
    'order'   => 'ASC',
    'exclude' => array($update_category, 1)
  ) );
  ?>

  <?php foreach( $categories as $category ): ?>
  <?php $cat = get_category( $category ); ?>
  <div class="post-item">
    <a href="/category/<?php echo $cat->slug; ?>">
      <?php echo $category->name; ?> <i class="fa fa-angle-right"></i>
    </a>
  </div>
  <?php endforeach; ?>
  <div class="post-item">
    <a href="/category/<?php echo get_category($update_category)->slug; ?>">
      <?php echo get_category($update_category)->name; ?> <i class="fa fa-angle-right"></i>
    </a>
  </div>
</div>
