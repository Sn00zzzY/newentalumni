<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
<div class="both"></div>
</div><!-- #content -->

<footer id="colophon" role="contentinfo">
    <div class="container">
        <div class="footer-inner">

            <div class="footer-links">


                <?php

                if( have_rows('ea_proud', option) ): ?>
                    <div class="proud-block">
                        EnterpriseAlumni is proud to be:

                        <div class="link-wrapper">
                            <?php while ( have_rows('ea_proud', option) ) : the_row();?>
                                <a href="<?php the_sub_field('ea_proud_link', option);?>" <?php echo substr(get_sub_field('ea_proud_link', option), 0, 4) == "http" ? 'target="_blank"' : '' ?>>
                                    <img src="<?php the_sub_field('ea_proud_image');?>" alt="logo">
                                </a>
                            <?php endwhile;?>
                        </div>

                    </div>
                <?php else :

                    // no rows found

                endif;

                ?>


                <div class="social-items">
                    <a target="_blank" href="<?= get_field('ea_social_facebook', option) ?>" class="social-facebook">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a target="_blank" href="<?= get_field('ea_social_twitter', option) ?>" class="social-twitter">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a target="_blank" href="<?= get_field('ea_social_linkedin', option) ?>" class="social-linkedin">
                        <i class="fa fa-linkedin"></i>
                    </a>
                </div>

                <?php wp_nav_menu(array(
                    'theme_location' => 'footer_menu',
                    'menu_id' => 'footer-nav',
                )); ?>
            </div>

            <p><?= get_field('ea_footer_copyright_text', option) ?></p>

        </div>
    </div>
</footer><!-- #colophon -->
<div id="contactModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span class="close-contact" data-dismiss="modal"></span>
                <h4 class="modal-title">Contact Us...</h4>
                <p>Fill in the details below and we will be in touch as soon as possible.</p>
            </div>
            <div class="modal-body">
                <?php echo do_shortcode(get_field('contact_form_modal', option)); ?>
            </div>
        </div>
    </div>
</div>

<div id="successModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span class="close-contact" data-dismiss="modal"></span>
                <h4 class="modal-title">Contact Us...</h4>
                <p>Fill in the details below and we will be in touch as soon as possible.</p>
            </div>
            <div class="modal-body">
                <div class="success-message">
                    <div class="image"></div>
                    <h3>Received!</h3>
                    <p>We will be in touch shortly</p>
                </div>
            </div>
        </div>
    </div>
</div>
</div><!-- .site-content-contain -->
</div><!-- #page -->
<?php wp_footer(); ?>

</body>
</html>
