<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="blog-page">
  <div class="blog-header blog-list-header parallax">
    <div class="container container-normal">
      <h2>Alumni Management blog </h2>
      <p>Business Alumni Insights, Best Practices & News</p>
    </div>
  </div>
  <div class="blog-content">
    <div class="container container-normal">
      <div class="blog-inner blog-list">
        <div class="row">
          <div class="col-sm-8 blog-left">
            <?php if ( have_posts() ) : ?>
              <div class="blog-items">

                <?php
                  // Start the Loop.
                  while ( have_posts() ) : the_post();
                    get_template_part( 'template-parts/post/content', get_post_format() );

                  // End the loop.
                  endwhile;
                ?>
              </div>
              <?php
              the_posts_pagination( array(
                  'prev_text' => twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous page', 'twentyseventeen' ) . '</span>',
                  'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'twentyseventeen' ) . '</span>' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ),
                  'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyseventeen' ) . ' </span>',
                ) );
            ?>
            <?php endif; ?>
          </div>
          <div class="col-sm-4 blog-right">
            <?php get_sidebar(); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php get_footer();

