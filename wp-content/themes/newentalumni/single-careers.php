<?php
/**
 * The template for displaying jobs and single jobs
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="job-page">
    <?php 
        while ( have_posts() ) : the_post();
      ?>
        <div class="blog-header">
            <div class="blog-back parallax" style="background-image: url(<?=get_template_directory_uri()?>/assets/images/careers-background-jpg.png)"></div>
            <div class="container container-normal">
                <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(1); ?>
                <?php the_title( '<h2>', '</h2>' ); ?>
                <h6><img src="<?php echo get_template_directory_uri(); ?>/assets/images/map.png" class="map" alt="location"/> <?php the_field('location'); ?> <span class="selector-head">|</span> Full Time <span class="selector-head">|</span> Posted: <?php echo get_the_date('m/d/Y'); ?></h6>
            </div>
        </div>
        <div class="blog-content">
            <div class="container container-normal">
                <div class="blog-inner">
                    <div class="blog-inside">
                        
                        <div class="career-header">
                            <h2>Job description - </h2>
                            <div class="social">
                                <span class="text-share-job">Share this job:</span>
                                <?php echo do_shortcode('[addtoany]')?>
                            </div>
                        </div>
                        <div class="content-holder">
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
</div>

<?php get_footer();
