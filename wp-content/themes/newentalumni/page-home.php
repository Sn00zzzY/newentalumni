<?php
/**
 * Template Name: Home Template
 */
get_header(); ?>

<main class="main">
	<?php get_template_part( 'template-parts/page/home', 'hero'); ?>
	<?php get_template_part( 'template-parts/page/home', 'what'); ?>
	<?php get_template_part( 'template-parts/page/home', 'video'); ?>
	<?php get_template_part( 'template-parts/page/home', 'why'); ?>
	<?php get_template_part( 'template-parts/page/home', 'which'); ?>
	<?php get_template_part( 'template-parts/page/home', 'who'); ?>
	<?php get_template_part( 'template-parts/page/home', 'other'); ?>
	<?php get_template_part( 'template-parts/page/alumni', 'contact'); ?>
</main>

<?php get_footer(); ?>