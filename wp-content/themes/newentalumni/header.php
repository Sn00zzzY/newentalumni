<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<link rel="apple-touch-icon" type="image/png" sizes="57x57" href="<?=get_template_directory_uri()?>/favicons/57.png">
<link rel="apple-touch-icon" type="image/png" sizes="60x60" href="<?=get_template_directory_uri()?>/favicons/60.png">
<link rel="apple-touch-icon" type="image/png" sizes="72x72" href="<?=get_template_directory_uri()?>/favicons/72.png">
<link rel="apple-touch-icon" type="image/png" sizes="76x76" href="<?=get_template_directory_uri()?>/favicons/76.png">
<link rel="apple-touch-icon" type="image/png" sizes="114x114" href="<?=get_template_directory_uri()?>/favicons/114.png">
<link rel="apple-touch-icon" type="image/png" sizes="120x120" href="<?=get_template_directory_uri()?>/favicons/120.png">
<link rel="apple-touch-icon" type="image/png" sizes="144x144" href="<?=get_template_directory_uri()?>/favicons/144.png">
<link rel="apple-touch-icon" type="image/png" sizes="152x152" href="<?=get_template_directory_uri()?>/favicons/152.png">
<link rel="apple-touch-icon" type="image/png" sizes="180x180" href="<?=get_template_directory_uri()?>/favicons/180.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?=get_template_directory_uri()?>/favicons/16.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?=get_template_directory_uri()?>/favicons/32.png">
<link rel="icon" type="image/png" sizes="48x48" href="<?=get_template_directory_uri()?>/favicons/48.png">
<link rel="icon" type="image/png" sizes="64x64" href="<?=get_template_directory_uri()?>/favicons/64.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?=get_template_directory_uri()?>/favicons/96.png">
<link rel="icon" type="image/png" sizes="128x128" href="<?=get_template_directory_uri()?>/favicons/128.png">
<link rel="icon" type="image/png" sizes="192x192" href="<?=get_template_directory_uri()?>/favicons/192.png">
<link rel="icon" type="image/png" sizes="194x194" href="<?=get_template_directory_uri()?>/favicons/194.png">
<link rel="icon" type="image/png" sizes="195x195" href="<?=get_template_directory_uri()?>/favicons/195.png">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentyseventeen' ); ?></a>

	<header id="masthead" role="banner">

		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-mainnav-collapse" aria-expanded="false">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">
                        <div class="logo-normal">
                            <img src="<?=get_template_directory_uri()?>/assets/images/logo.png">
                        </div>
                        <div class="logo-sticky">
                            <img src="<?=get_template_directory_uri()?>/assets/images/logo-color.png">
                        </div>
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-mainnav-collapse">
                    <?php wp_nav_menu( array(
                        'theme_location' => 'top',
                        'menu_id'        => 'top-menu',
                    ) ); ?>
                    <!-- <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="\">Home</a></li>
                        <li><a href="services.html">News</a></li>
                        <li><a href="project.html">Integrations</a></li>
                        <li><a href="safety.html">2017 Alumni Survey</a></li>
                        <li><a href="location.html">Contact</a></li>
                    </ul> -->
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
	</header><!-- #masthead -->

    <?php 

	/*
	 * If a regular post or page, and not the front page, show the featured image.
	 * Using get_queried_object_id() here since the $post global may not be set before a call to the_post().
	 */
	?>

	<div class="site-content-contain">
		<div id="content">
