<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="blog-page">
  <?php 
    while ( have_posts() ) : the_post();
  ?>
    <div class="blog-header" id="post-<?php the_ID(); ?>">
      <div class="blog-back parallax" style="<?php if (is_single() && get_the_post_thumbnail_url()) :?> background-image: url(<?php echo get_the_post_thumbnail_url(); ?>) <?php endif; ?>"></div>
      <div class="container container-normal">
        <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
        <?php the_title( '<h2>', '</h2>' ); ?>
        <h6>
          by <a href="#"><?php echo get_the_author(); ?></a> 
          <?php 
            $cat = get_the_category(); 
            $cat = $cat[0];
            echo 'in ' . $cat->name;
          ?>
        </h6>
      </div>
    </div>
    <div class="blog-content">
      <div class="container container-normal">
        <div class="blog-inner">
          <div class="row">
            <div class="col-sm-8 blog-left">
              <div class="blog-inside">
                <div class="blog-image" style="<?php if (is_single() && get_the_post_thumbnail_url()) :?> background-image: url(<?php echo get_the_post_thumbnail_url(); ?>) <?php endif; ?>">
                </div>
                <div class="blog-date">
                  <span class="publish-date">Published: <?php echo get_the_date('jS F Y'); ?></span>
                  <div class="social">
                    <?php echo do_shortcode('[addtoany]')?>
                  </div>
                </div>
                <?php
                  the_content();
                ?>
              </div>
            </div>
            <div class="col-sm-4 blog-right">
              <?php get_sidebar(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php endwhile; ?>
</div>

<?php get_footer();
