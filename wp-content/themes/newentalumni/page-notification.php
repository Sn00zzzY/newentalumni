<?php
/**
 * Template Name: Notification Template
 */
get_header(); ?>

<main class="main">
  <?php get_template_part( 'template-parts/page/notification', 'hero'); ?>
  <?php get_template_part( 'template-parts/page/notification', 'content'); ?>
</main>

<?php get_footer(); ?>
