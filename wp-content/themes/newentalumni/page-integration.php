<?php
/**
 * Template Name: Integration Template
 */
get_header(); ?>

<main class="main page-integration">
	<?php get_template_part( 'template-parts/page/integration', 'hero'); ?>
	<?php get_template_part( 'template-parts/page/integration', 'feature'); ?>
</main>

<?php get_footer(); ?>